<?php include("customwork-template.php"); ?>

<?php

// INDICE

// $categoria_i =      get_post_custom_values( $key = "categoria_i_rsc_acf" );
// $categoria_ii =     get_post_custom_values( $key = "categoria_ii_rsc_acf" );
// $preferencia =      get_post_custom_values( $key = "preferencia_del_autor_rsc_acf" );
// $socio_amg =        get_post_custom_values( $key = "el_autor_principal_es_socio_de_la_amg_rsc_acf" );
$nombre =           get_post_custom_values( $key = "nombre_del_autor_principal_rsc_acf" );
$correo =           get_post_custom_values( $key = "correo_del_autor_principal_rsc_acf" );
$institucion =      get_post_custom_values( $key = "institucion_del_autor_principal_rsc_acf" );
$ciudad =           get_post_custom_values( $key = "ciudad_rsc_acf" );
$tiene_coautores =  get_post_custom_values( $key = "el_trabajo_tiene_coautores_rsc_acf" );
$coautores =        get_post_custom_values( $key = "coautores_rsc_acf" );
$antecedentes =     get_post_custom_values( $key = "antecedentes_rsc_acf" );
$objetivo =         get_post_custom_values( $key = "objetivo_rsc_acf" );
$series =           get_post_custom_values( $key = "serie_de_casos_rsc_acf" );
$discusion =        get_post_custom_values( $key = "discusion_rsc_acf" );
$conclusiones =     get_post_custom_values( $key = "conclusiones_rsc_acf" );
$financiamiento =   get_post_custom_values( $key = "financiamiento_rsc_acf" );
$a_figura =         get_post_custom_values( $key = "adjuntar_figura_rsc_acf" );
$a_tabla =          get_post_custom_values( $key = "adjuntar_tabla_rsc_acf" );

?>

<body>

    <div class="worksheet-heading noprint">
        <h1><?php the_title(); ?></h1>
        <button onclick="myFunction()">Ver en PDF</button>
        <!-- <div class="worksheet-panel" style="padding: 20px; background-color: white; text-align: center;">
        </div> -->
    </div>

    <div class="worksheet animated fadeIn">
        <p>
            <span>Título del trabajo:</span><?php the_title_attribute(); ?><br><span>Autor:</span> <?php echo $nombre[0];?> <span>Coautores:</span> <?php if( have_rows('coautores_rio_acf')){ while ( have_rows('coautores_rio_acf') ) : $row = the_row(); the_sub_field('coautores'); echo ", "; endwhile; } else { echo "Sin Coautores"; } ?> <span>Institución:</span> <?php echo $institucion[0];?> <span>Ciudad:</span> <?php echo $ciudad[0]; ?> <span>Correo:</span> <?php echo $correo[0];?><br><span>Antecedentes:</span> <?php echo $antecedentes[0];?><br><span>Objetivo:</span> <?php echo $objetivo[0];?><br><span>Serie de Casos:</span> <?php echo $series[0];?><br><span>Discusión:</span> <?php echo $discusion[0];?><br><span>Conclusiones:</span><?php echo $conclusiones[0];?><br><span>Financiamiento:</span><?php echo $financiamiento[0];?>
        </p>   
    </div>
    <div class="worksheet animated fadeIn">
        <p>
            <span>Imagen:<br></span>

            <?php
                if ($a_figura[0]){
                    $workImage = $a_figura[0];
                    ?>
                    <a href="<?php echo wp_get_attachment_url($a_figura[0]); ?>"><?php echo wp_get_attachment_image($a_figura[0]); ?></a>
                    <?php
                } else {
                    echo "Este trabajo no contiene imagen.";
                }
            ?>
            <br>
            <span>Tabla:<br></span>
            <?php
                if ($a_tabla[0]) {
                $workTable = $a_tabla[0];
                ?>
                    <a href="<?php echo wp_get_attachment_url($a_tabla[0]); ?>">Ver Tabla</a>
                <?php
                } else {
                    echo "Este trabajo no contiene tabla.";
                }
            ?>
        </p>   
    </div>

    <?php wp_footer(); ?>

    <script type="text/javascript">
        function myFunction() {
            window.print();
        }    
    </script>
</body>
</html>