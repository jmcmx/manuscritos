<?php include("customwork-template.php"); ?>

<?php

// INDICE

// $categoria_i =      get_post_custom_values( $key = "categoria_i_rio_acf" );
// $categoria_ii =     get_post_custom_values( $key = "categoria_ii_rio_acf" );
// $preferencia =      get_post_custom_values( $key = "preferencia_del_autor_rio_acf" );
// $socio_amg =        get_post_custom_values( $key = "el_autor_principal_es_socio_de_la_amg_rio_acf" );
$nombre =           get_post_custom_values( $key = "nombre_del_autor_principal_rio_acf" );
$correo =           get_post_custom_values( $key = "correo_del_autor_principal_rio_acf" );
$institucion =      get_post_custom_values( $key = "institucion_del_autor_principal_rio_acf" );
$ciudad =           get_post_custom_values( $key = "ciudad_rio_acf" );
$tiene_coautores =  get_post_custom_values( $key = "el_trabajo_tiene_coautores_rio_acf" );
$coautores =        get_post_custom_values( $key = "coautores_rio_acf" );
$antecedentes =     get_post_custom_values( $key = "antecedentes_rIo_acf" );
$objetivo =         get_post_custom_values( $key = "objetivo_rio_acf" );
$material =         get_post_custom_values( $key = "material_y_metodos_rio_acf" );
$resultados =       get_post_custom_values( $key = "resultados_rio_acf" );
$conclusiones =     get_post_custom_values( $key = "conclusiones_rio_acf" );
$financiamiento =   get_post_custom_values( $key = "financiamiento_rio_acf" );
$a_figura =         get_post_custom_values( $key = "adjuntar_figura_rio_acf" );
$a_tabla =          get_post_custom_values( $key = "adjuntar_tabla_rio_acf" );

?>

<body>

    <div class="worksheet-heading noprint">


        <!-- <div style="background: white;"> -->
        <?php
            // $post_author_id = get_post_field( 'post_author', $post_id );
            
            // echo $post_author_id." ,";
            // echo get_the_author_meta( 'user_email', $post_author_id )." ,";
            // echo get_the_author_meta( 'first_name', $post_author_id )."<br><br>";
            // var_dump($ciudad);
            
            // $manus_Editor        = get_post_custom_values( $key = "asignar_editor" );
            // $manus_EditorName    = get_the_author_meta( 'first_name', $manus_Editor[0] );
            // $manus_EditorEmail   = get_the_author_meta( 'user_email', $manus_Editor[0] );

            // $manus_Revisor        = get_post_custom_values( $key = "asignar_revisor" );
            // $manus_RevisorName    = get_the_author_meta( 'first_name', $manus_Revisor[0] );
            // $manus_RevisorEmail   = get_the_author_meta( 'user_email', $manus_Revisor[0] );

            // $manus_AuthorEmail   = get_the_author_meta( 'user_email', $manus_AuthorId );
            // $manus_AuthorName    = get_the_author_meta( 'first_name', $manus_AuthorId );

            // echo $manus_Editor[0]."<br>";
            // echo "llega Editor: ".$manus_EditorName." email: ".$manus_EditorEmail;
            // echo "llega Revisor: ".$manus_RevisorName." email: ".$manus_RevisorEmail;
        ?>
        <!-- </div> -->


        <h1><?php the_title(); ?></h1>
        <button onclick="myFunction()">Ver en PDF</button>
        <!-- <div class="worksheet-panel" style="padding: 20px; background-color: white; text-align: center;">
        </div> -->
    </div>

    <div class="worksheet animated flipInY">
        <p>
            <span>Título del trabajo:</span><?php the_title_attribute(); ?><br><span>Autor:</span> <?php echo $nombre[0];?> <span>Coautores:</span> <?php if( have_rows('coautores_rio_acf')){ while ( have_rows('coautores_rio_acf') ) : $row = the_row(); the_sub_field('coautores'); echo ", "; endwhile; } else { echo "Sin Coautores"; } ?> <span>Institución:</span> <?php echo $institucion[0];?> <span>Ciudad:</span> <?php echo $ciudad[0]; ?> <span>Correo:</span> <?php echo $correo[0];?><br><span>Antecedentes:</span> <?php echo $antecedentes[0];?><br><span>Objetivo:</span> <?php echo $objetivo[0];?><br><span>Material y Métodos:</span> <?php echo $material[0];?><br><span>Resultados:</span> <?php echo $resultados[0];?><br><span>Conclusiones:</span> <?php echo $conclusiones[0];?><br><span>Financiamiento:</span> <?php echo $financiamiento[0];?>
        </p>   
    </div>
    <div class="worksheet animated flipInY">
        <p>
            <span>Imagen:<br></span>
            <?php
                if ($a_figura[0]){
                    $workImage = $a_figura[0];
                    ?>
                    <a href="<?php echo wp_get_attachment_url($a_figura[0]); ?>"><?php echo wp_get_attachment_image($a_figura[0]); ?></a>
                    <?php
                } else {
                    echo "Este trabajo no contiene imagen.";
                }
            ?>
            <br>
            <span>Tabla:<br></span>
            <?php
                if ($a_tabla[0]) {
                $workTable = $a_tabla[0];
                ?>
                    <a href="<?php echo wp_get_attachment_url($a_tabla[0]); ?>">Ver Tabla</a>
                <?php
                } else {
                    echo "Este trabajo no contiene tabla.";
                }
            ?>
        </p>    
    </div>

    <?php wp_footer(); ?>

    <script type="text/javascript">
        function myFunction() {
            window.print();
        }    
    </script>
</body>
</html>