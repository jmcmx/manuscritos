
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title(); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>
    </head>

<?php

// INDICE

$categoria_i =      get_post_custom_values( $key = "categoria_i_rio_acf" );
$categoria_ii =     get_post_custom_values( $key = "categoria_ii_rio_acf" );
$preferencia =      get_post_custom_values( $key = "preferencia_del_autor_rio_acf" );
$socio_amg =        get_post_custom_values( $key = "el_autor_principal_es_socio_de_la_amg_rio_acf" );
$nombre =           get_post_custom_values( $key = "nombre_del_autor_principal_rio_acf" );
$correo =           get_post_custom_values( $key = "correo_del_autor_principal_rio_acf" );
$institucion =      get_post_custom_values( $key = "institucion_del_autor_principal_rio_acf" );
$tiene_coautores =  get_post_custom_values( $key = "el_trabajo_tiene_coautores_rio_acf" );
$coautores =        get_post_custom_values( $key = "coautores_rio_acf" );
$antecedentes =     get_post_custom_values( $key = "antecedentes_rIo_acf" );
$objetivo =         get_post_custom_values( $key = "objetivo_rio_acf" );
$material =         get_post_custom_values( $key = "material_y_metodos_rio_acf" );
$resultados =       get_post_custom_values( $key = "resultados_rio_acf" );
$conclusiones =     get_post_custom_values( $key = "conclusiones_rio_acf" );
$financiamiento =   get_post_custom_values( $key = "financiamiento_rio_acf" );
$a_figura =         get_post_custom_values( $key = "adjuntar_figura_rio_acf" );
$a_tabla =          get_post_custom_values( $key = "adjuntar_tabla_rio_acf" );

?>


<style>
    p {
        font-size:12pt;
        line-height:16pt;
        font-family: Arial;
    }
    span {
        font-weight: bold;
    }
</style>

<div class="col-xs-12">
    <p>
        <span>Título del trabajo:</span><?php the_title_attribute(); ?><br><span>Autor:</span> <?php echo $nombre[0];?> <span>Coautores:</span> <?php echo $coautores[0];?> <span>Institución:</span> <?php echo $institucion[0];?> <span>Correo:</span> <?php echo $correo[0];?><br><span>Antecedentes:</span> <?php echo $antecedentes[0];?><br><span>Objetivo:</span> <?php echo $objetivo[0];?><br><span>Material y Métodos:</span> <?php echo $material[0];?><br><span>Resultados:</span> <?php echo $resultados[0];?><br><span>Conclusiones:</span> <?php echo $conclusiones[0];?>
    </p>
</div>

<?php wp_footer(); ?>
</body>
</html>