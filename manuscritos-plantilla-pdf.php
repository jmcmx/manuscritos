<?php /* Template Name: Manuscritos */ ?>

<?php

// INDICE

$categoria_i = 		get_post_custom_values( $key = "Categoría I" );
$categoria_ii = 	get_post_custom_values( $key = "Categoría II" );
$preferencia = 		get_post_custom_values( $key = "Preferencia del Autor" );
$socio_amg = 		get_post_custom_values( $key = "El autor principal es socio de la AMG?" );
$nombre = 			get_post_custom_values( $key = "Nombre del Autor Principal" );
$correo = 			get_post_custom_values( $key = "Correo del autor principal" );
$institucion = 		get_post_custom_values( $key = "Institución del Autor Principal" );
$tiene_coautores =	get_post_custom_values( $key = "El trabajo tiene Coautores?" );
$coautores = 		get_post_custom_values( $key = "Coautores" );
$antecedentes = 	get_post_custom_values( $key = "Antecedentes" );
$objetivo = 		get_post_custom_values( $key = "Objetivo");
$material = 		get_post_custom_values( $key = "Material y Métodos" );
$resultados = 		get_post_custom_values( $key = "Resultados" );
$conclusiones = 	get_post_custom_values( $key = "Conclusiones" );
$financiamiento = 	get_post_custom_values( $key = "Financiamiento" );
$a_figura = 		get_post_custom_values( $key = "Adjuntar Figura" );
$a_tabla = 			get_post_custom_values( $key = "Adjuntar Tabla" );

?>

<style>
	p {
		font-size:12pt;
		line-height:16pt;
		font-family: Arial;
	}
	span {
		font-weight: bold;
	}
</style>


	<div class="col-xs-12">
		<p>
			<span>Título del trabajo:</span> <?php echo $categoria_i[0];?> <span>Categoría I:</span> <?php echo $categoria_i[0];?> <span>Categoría II:</span> <?php echo $categoria_ii[0];?> <span>Preferencia del Autor:</span> <?php echo $preferencia[0];?> <span>El Autor principal es socio de la AMG?:</span> <?php echo $socio_amg[0];?> <span>Nombre del Autor Principal:</span> <?php echo $nombre[0];?> <span>Correo del autor:</span> <?php echo $correo[0];?> <span>Institución principal del autor:</span> <?php echo $institucion[0];?> <span>El trabajo tiene Coautores?:</span> <?php echo $tiene_coautores[0];?> <span>Coautores:</span> <?php echo $coautores[0];?> <span>Antecedentes:</span> <?php echo $antecedentes[0];?> <span>Objetivo:</span> <?php echo $objetivo[0];?> <span>Material y Métodos:</span> <?php echo $material[0];?> <span>Resultados:</span> <?php echo $resultados[0];?> <span>Conclusiones:</span> <?php echo $conclusiones[0];?> <span>Financiamiento:</span> <?php echo $financiamiento[0];?> <span>Adjuntar Figura:</span> <?php echo $a_figura[0];?> <span>Adjuntar Tabla:</span> <?php echo $a_tabla[0];?>
		</p>
	</div>



