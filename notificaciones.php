<?php 
$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "autor", 
		'from' => "default", 
		'subject' => "AMG publicaciones: confirmación de envío / Submission confirmation", 
		'message' => "
Estimado/a {{name}}:

Le confirmamos la recepción del artículo titulado: '{{post-title}}', que nos ha enviado para su posible publicación en el Supl. 1 de la Revista de Gastroenterología de México.

Usted podrá observar en el sistema el proceso de su resumen en su estatus editorial de su trabajo.

En breve se iniciará el proceso de revisión de su resumen. En caso de que sea necesario que haga algún cambio, también se le notificará por correo electrónico.

No dude en contactar con la redacción para cualquier información adicional.

Reciba un cordial saludo,

AMG
Revista de Gastroenterología de México
		", 
		'status' => "Nuevo envío"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "admin", 
		'from' => "default", 
		'subject' => "RGMX: entrada nuevo manuscrito", 
		'message' => "
Hola, {{name}}:

Ha llegado un artículo nuevo a la plataforma de AMG manuscritos Revista de Gastroenterología de México.

Puedes verlo en este link: {{blog-url}}
Usuario: {{user-mail}}


Un saludo,

Asociación Mexicana de Gastroenterología
Revista de Gastroenterología de México
		", 
		'status' => "Nuevo envío"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "autor", 
		'from' => "default", 
		'subject' => "AMG publicaciones: envío artículo / petición de cambios", 
		'message' => "
Estimado/a {{name}}:

Muchas gracias por colaborar enviando un artículo a nuestra revista.

Antes de empezar con el proceso editorial, les solicitamos que realicen las siguientes modificaciones:

{{custom-message}}

Para poder realizar los cambios que le indicamos, debe acceder a la dirección {{post-url}} e introducir sus datos de registro:

Usuario: {{user-mail}}

Si no sabe o no recuerda su Password, entre en:

{{blog-url}}/wp-login.php?action=lostpassword

Cuando haya entrado en la web, haga clik en "editar" realice los cambios necesarios en el texto y de clik en Finalizar Registro  para que llegue  a la asistente editorial.

En espera de sus noticias, reciba un cordial saludo,
 
Asociación Mexicana de Gastroenterología
Revista de Gastroenterología de México
		", 
		'status' => ""
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "autor", 
		'from' => "default", 
		'subject' => "AMG publicaciones: confirmación de envío / Submission confirmation", 
		'message' => "
Estimado/a {{name}}:

Le confirmamos la recepción del artículo titulado: '{{post-title}}', que nos ha enviado para su posible publicación en el Supl. 2 de la Revista de Gastroenterología de México.

Usted podrá observar en el sistema el proceso de su resumen en su estatus editorial de su trabajo.

En breve se iniciará el proceso de revisión de su resumen. En caso de que sea necesario que haga algún cambio, también se le notificará por correo electrónico.

No dude en contactar con la redacción para cualquier información adicional.

Reciba un cordial saludo,

Asociación Mexicana de Gastroenterología
Revista de Gastroenterología de México
		", 
		'status' => ""
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "admin", 
		'from' => "default", 
		'subject' => "AMG manuscaaritos: El autor ha hecho los cambios pedidos en la revisión técnica", 
		'message' => "
Estimado/a {{name}}:

El autor del resumen '{{post-title}}': ha aceptado los cambios que le indicaste tras la revisión técnica ("Technical Check") de su resumen. El manuscrito ha vuelto a su pantalla de inicio para seguir con elproceso.

Un saludo,

Asociación Mexicana de Gastroenterología
Revista de Gastroenterología de México
		", 
		'status' => "Nuevo envío"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "autor", 
		'from' => "default", 
		'subject' => "{{post-id}}: código de referencia de su artículo / Your Submission Ref. No.", 
		'message' => "
Estimado/a {{name}}:

Le confirmamos que se ha iniciado el proceso de revisión de su artículo {{post-title}}: (ref. {{post-id}}), enviado para su posible publicación en el Supl. 2, de la Revista de Gastroenterología de México.

Para consultar el estado de su artículo debe acceder a la página y revisar el estatus editorial de su trabajo:

Muchas gracias por el interés mostrado por nuestra revista.

Reciba un cordial saludo,

Ma. de Lourdes Torres
Asistente editorial
Revista de Gastroenterología de México
		", 
		'status' => "Pendiente de editor"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "editor", 
		'from' => "default", 
		'subject' => "{{post-id}}: Nuevo artículo asignado / new assignment", 
		'message' => "
Estimado/a {{name}}:

Te enviamos un artículo nuevo para que te hagas cargo de él. El título es {{post-title}}: (Ref. {{post-id}}).

Puedes entrar a verlo directamente con el siguiente link {{post-edit}} o con tus claves en esta dirección: {{blog-url}}

Muchas gracias, 

Ma. de Lourdes Torres
Asistente editorial
Revista de Gastroenterología de México
		", 
		'status' => "Pendiente de editor"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "revisor", 
		'from' => "default", 
		'subject' => "AMG manuscritos: Recordatorio de revisión", 
		'message' => "
Apreciado/a {{name}}:

Hemos recibido el trabajo '{{post-title}}', de referencia {{post-id}}, para su publicación en el Supl. 2 de la Revista de Gastroenterología de México. Le agradeceríamos que nos diera su calificación y recomendación si el trabajo amerita ser  oral, cartel, aceptado  y/o rechazado.

Con el fin de no retrasar el proceso de revisión del artículo, le agradeceremos enormemente que confirme su disponibilidad para efectuar la evaluación mediante los siguientes enlaces:

- Para ACEPTAR o RECHAZAR la propuesta de evaluación dé clic aquí: {{post-edit-url}}
- Para ACEPTAR la propuesta de evaluación, haga clic en este link: {{post-accept}} 
- Para RECHAZARLA, haga clic en: {{post-deny}}

Le recordamos su compromiso de realizar la revisión dentro del plazo establecido de una semana (7 días) 

Este manuscrito es propiedad de los autores. Si consulta a otros revisores, por favor, proteja la privacidad del documento. Le agradecemos por anticipado su contribución al proceso editorial y su rápida evaluación, y quedamos a su disposición.

Atentamente,
(Aquí el sistema debe jalar el nombre del editor que tenga a su cargo el trabajo) "<p>MERGE TAG PENDIENTE</p>"
Dr. José María Remes Troche "<p>EJEMPLO</p>"
Editor Titular
Revista de Gastroenterología de México
		", 
		'status' => "Pendiente de revisor"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "revisor", 
		'from' => "default", 
		'subject' => "Ref. {{post-id}}: revisión", 
		'message' => "
Estimado/a {{name}}:

El pasado día "MERGE TAG PENDIENTE" le enviamos un trabajo para su revisión “{{post-title}}”:(Ref. {{post-id}}), que habíamos recibido en Revista de Gastroenterología de México para su posible publicación en el suplemento 1 de la Revista de Gastroenterología de México.

No hemos recibido todavía la confirmación de su disponibilidad, quizá porque nuestra petición no le llegó correctamente.

- Para ACEPTAR o RECHAZAR la propuesta de evaluación dé clic aquí: {{post-edit-url}}
- Para ACEPTAR la propuesta de evaluación, haga clic en este link: {{post-accept}} 
- Para RECHAZARLA, haga clic en: {{post-deny}}

Si los enlaces anteriores no funcionan, entre por favor en "MERGE TAG PENDIENTE" con las siguientes claves:

Your username is: {{user-mail}}
Si no sabe o no recuerda su Password, entre en: {{blog-url}}/wp-login.php?action=lostpassword

Le agradeceremos que nos envíe sus valiosas sugerencias y calificaciones en el plazo de una semana (7 días).

Un cordial saludo,

Ma.  de Lourdes Torres
Asistente editorial
Revista de Gastroenterología de México
		", 
		'status' => "Pendiente de revisor"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "revisor", 
		'from' => "default", 
		'subject' => "Ref. {{post-id}}: revisión de artículo", 
		'message' => "
Estimado/a {{name}}:

Muchas gracias por aceptar realizar la evaluación del trabajo “{{post-title}}”  (Ref. {{post-id}}). 

Para descargarse el manuscrito, acceda al enlace "MERGE TAG PENDIENTE" o bien entre directamente en su pantalla de inicio.

Para enviar sus comentarios, haga clic en la opción "MERGE TAG PENDIENTE" (también en el menú "Action Links" de la columna "Action").

"CHECAR EL TEXTO EN VERDE CON EL SISTEMA"

Le agradeceríamos recibir sus comentarios antes del próximo "MERGE TAG PENDIENTE".

Muchas gracias por su colaboración.

Reciba un cordial saludo,


"MERGE TAG PENDIENTE"
Revista de Gastroenterología de México
		", 
		'status' => ""
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "revisor", 
		'from' => "default", 
		'subject' => "Ref. {{post-id}}: Recordatorio / Evaluación", 
		'message' => "
Estimado/a {{name}}:

Le recordamos que estamos a la espera de recibir sus comentarios del artículo '{{post-title}}''  (Ref. {{post-id}}), cuya fecha de entrega era el pasado "MERGE TAG PENDIENTE". 

Para evitar que el proceso de revisión se extienda demasiado, le rogamos que nos envíe sus comentarios lo antes posible. 

Puede acceder ahora al manuscrito a través del link: {{post-edit}} o entrar con sus datos de acceso en {{blog-url}}

Reciba un cordial saludo,

María de Lourdes Torres
Asistente editorial
Revista de Gastroenterología de México
AMG

Si tiene alguna duda sobre el uso del programa, póngase en contacto con el departamento de Ayuda-EES por correo electrónico (ayuda-ees@elsevier.com).
		", 
		'status' => "Pendiente de revisor"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "revisor", 
		'from' => "default", 
		'subject' => "{{post-id}}: acuse de recibo de sus comentarios", 
		'message' => "
Estimado/a {{name}}:

Acusamos recibo de su calificación y recomendación del trabajo: '{{post-title}}' (Ref. {{post-id}}).

Le recordamos que podrá consultar la decisión editorial sobre este manuscrito, en la columna de estatus editorial de su pantalla en el siguiente link: "MERGE TAG PENDIENTE"

Muchas gracias por el entusiasmo y apoyo brindado,

Atentamente,

Dr. José María Remes Troche "MERGE TAG PENDIENTE"
Revista de Gastroenterología de México
AMG
		", 
		'status' => "Evaluación entregada"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "editor", 
		'from' => "default", 
		'subject' => "{{post-id}}: "MERGE TAG PENDIENTE" ha entregado sus comentarios", 
		'message' => "
Estimado/a {{name}}:

"MERGE TAG PENDIENTE", M.D. ha entregado su evaluación del artículo '{{post-title}}' (Ref. {{post-id}}).

Puede entrar a ver los comentarios del revisor en este enlace "confirmar" {{post-edit}} o bien en {{blog-url}} introduciendo sus claves. 


Un saludo,

Asociación Mexicana de Gastroenterología
Revista de Gastroenterología de México
		", 
		'status' => "Evaluaciones entregadas"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "editor", 
		'from' => "default", 
		'subject' => "Ref. {{post-id}}: evaluaciones completas / completed reviews", 
		'message' => "
Estimado/a {{name}}:

Le informamos de que los revisores invitados a evaluar el manuscrito '{{post-title}}' (Ref. {{post-id}}) han entregado sus comentarios.

Puede acceder a ellos a través del siguiente link: "confirmar" {{post-edit}}.

Atentamente,

Asociación Mexicana de Gastroenterología

Ma. De Lourdes Torres 
Asistente editorial
Revista de Gastroenterología de México
		", 
		'status' => "Evaluaciones entregadas"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "editor", 
		'from' => "default", 
		'subject' => "Ref. {{post-id}}: el editor ha decidido", 
		'message' => "
Hola, Ma. de Lourdes"MERGE TAG PENDIENTE":

"MERGE TAG PENDIENTE" (El sistema debe jalar el nombre del editor a cargo). Jesus Yamamoto-Furusho, M.D., M.Sc, Ph.D. ha tomado una decisión sobre el manuscrito '{{post-title}}' (Ref. {{post-id}}). Su decisión es: "MERGE TAG PENDIENTE"(aquí cambia según sea el caso, Revisi´´on menor, Revisión Mayor, Aceptado y Rechazado).

Puedes acceder al manuscrito a través de este link: "confirmar" {{post-edit}}

Atentamente,

Asociación Mexicana de Gastroenterología
Revista de Gastroenterología de México
		", 
		'status' => "Decisión en curso"
	);


$amgNotificaciones = array(
		'title' => "", 
		'description' => "", 
		'to' => "autor", 
		'from' => "default", 
		'subject' => "{{post-id}}: decisión de los editores / editorial decision", 
		'message' => "
Estimado/a "es el mismo merge tag para editores, revisores, autores y admin"{{name}}:

Le comunicamos que su manuscrito '{{post-title}}' (Ref. {{post-id}}) ha sido aceptado para su publicación en el Suplemento 1, de la Revista de Gastroenterología de México.

Reciba un cordial saludo,

Ma. de Lourdes Torres
Asistente editorial
Revista de Gastroenterología de México
		", 
		'status' => "Aceptado"
	);


 ?>