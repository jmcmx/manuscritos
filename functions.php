<?php
include_once( 'modules/analytics.php' );
include_once( 'modules/admin-menu.php' );
include_once( 'modules/counter-shortcode.php' );
include_once( 'modules/add-columns.php' );
include_once( 'modules/main.php' );
include_once( 'modules/update-publish.php' );
include_once( 'modules/user-profile.php' );
// include_once( 'modules/new-row-actions.php' );
// include_once( 'modules/publish-alert.php' );
\Manuscritos\Modules\Main::init();

// - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ADD SCRIPT JS
// - - - - - - - - - - - - - - - - - - - - - - - - - - -

// REMOVE QUICK EDIT

function remove_quick_edit( $actions ) {
    unset($actions['inline hide-if-no-js']);
    return $actions;
  }

  add_filter('post_row_actions','remove_quick_edit',10,1);

// CAMBIO DE OPCIONES DE INDEX DE CUSTOM POST TYPES

add_filter( 'post_row_actions', 'modify_list_row_actions', 10, 2 );
 
function modify_list_row_actions( $actions, $post ) {

    $post_type_array = array('invoriginal', 'trabajoslibresrsc', 'trabajoslibresrio', 'mistrabajos');    // Check for your post type.
  
    if ( $post->post_type == "invoriginal" || $post->post_type == "trabajoslibresrsc" || $post->post_type == "trabajoslibresrio" || $post->post_type == "mistrabajos" ) {
 
        // Build your links URL.
        $url = admin_url( 'post.php?post=' . $post->ID .'&action=edit' );
 
        // Maybe put in some extra arguments based on the post status.
        $assign_url = get_stylesheet_directory_uri()."/tester.php?post=". $post->ID .'&action=assign';
        $scores_url = get_stylesheet_directory_uri()."/scores.php?post=". $post->ID .'&action=assign';
        $viewpost_url = get_permalink($post->ID);
        $mails_url = get_bloginfo('url')."/wp-admin/admin.php?page=wpml_plugin_log";
        $edit_link = add_query_arg( array( 'action' => 'edit' ), $url );
 
        // The default $actions passed has the Edit, Quick-edit and Trash links.
        $trash = $actions['trash'];
        $view = $actions['view'];
 
        /*
         * You can reset the default $actions with your own array, or simply merge them
         * here I want to rewrite my Edit link, remove the Quick-link, and introduce a
         * new link 'Copy'
         */

        if( current_user_can('manage_options') ) {
          $actions = array(
              'edit' => sprintf( '<a href="%1$s">Editar</a>',
              esc_url( $edit_link ) ),
              'trash' => $trash,
              'view' =>  sprintf( '<a href="%1$s" target="_blank">Ver PDF</a>', esc_url( $viewpost_url ) ),
              'emails' =>  sprintf( '<a href="%1$s" target="_blank">Ver Mails</a>', esc_url( $mails_url ) ),
              'asignar' => sprintf( '<a href="%1$s" target="_blank">Asignar Trabajo</a>', esc_url( $assign_url ) ), 
              'score' => sprintf( '<a href="%1$s" target="_blank">Calificaciones</a>', esc_url( $scores_url ) )
          );
        } else {
        }

 
       
    }
 
    return $actions;
}
 
// REMOVE SUBSUBSUB FOR AUTHOR

/*
 *Remove the post information i.e subsubsub information. 
 */

function remove_sub_sub_posts_filter() {
  global $current_user;
 if($current_user -> roles[0] == 'author'){
echo '
<style>ul.subsubsub .all,
ul.subsubsub .publish,
ul.subsubsub .sticky,
ul.subsubsub .trash,
ul.subsubsub .draft,
ul.subsubsub .pending,
.view-switch,
.tablenav ,
.row-actions .editinline,
.check-column
* {display:none;}</style>
';
  }
}

add_action('admin_head', 'remove_sub_sub_posts_filter');


// CAMBIO DE LABELS
 
add_action( 'admin_enqueue_scripts', 'equeue_extra_scripts' );

// FIX DE COMENTARIOS
// The7 agrega su propio botón de comentarios, este no funciona con el plugin Comment Attachment,
// con extra.css se elimina el botón del tema y se regresa el botón default de wordpress que sí funciona.

add_action( 'wp_print_scripts', 'equeue_extra_scripts' );

function equeue_extra_scripts() {
  wp_enqueue_script( 'mysite-scripts', get_stylesheet_directory_uri()."/js/extra.js" );
  wp_enqueue_script( 'mysite-counter', get_stylesheet_directory_uri()."/js/contador.js", array(), false, true );
  wp_enqueue_style( 'mysite-styles', get_stylesheet_directory_uri()."/css/extra.css" );
}

// ADD SUB & SUPERSCRIPT TO TINYMCE

function my_mce_buttons_2( $buttons ) {	
	/**
	 * Add in a core button that's disabled by default
	 */
	$buttons[] = 'superscript';
	$buttons[] = 'subscript';

	return $buttons;
}
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

// DISPLAY ROLE OF CURRENT USER Y NAVBAR

function wpse_203917_admin_bar_menu( $wp_admin_bar ) {
    if ( ! $node = $wp_admin_bar->get_node( 'my-account' ) )
        return;

    $roles = wp_get_current_user()->roles;

    $node->title .= sprintf( ' (%s)', implode( ', ', $roles ) );

    $wp_admin_bar->add_node( $node );
}

add_action( 'admin_bar_menu', 'wpse_203917_admin_bar_menu' );


// DISABLE VIEW NAME

add_action('admin_head', 'manuscritos_editor_disable_fields');
function manuscritos_editor_disable_fields() {
  if( current_user_can('edit_others_pages') && !current_user_can('manage_options') ) { 
    wp_enqueue_script( 'mysite-new-scripts', get_stylesheet_directory_uri()."/js/disable.js" );
    add_filter( 'tiny_mce_before_init', function( $args ) {
        // do you existing check for published here
        if ( 1 == 1 )
             $args['readonly'] = 1;
        return $args;
    } );
  }
}

// AGREGAR LISTA DE MIS TRABAJOS A DASHBOARD WIDGET

add_action( 'wp_dashboard_setup', 'register_my_dashboard_widget_mysubmissions' );
function register_my_dashboard_widget_mysubmissions() {
    wp_add_dashboard_widget(
        'my_dashboard_widget',
        'Mis trabajos inscritos',
        'my_dashboard_mysubmissions_widget_display'
    );

}

function my_dashboard_mysubmissions_widget_display() {
    $site_url = home_url();

    if ( is_user_logged_in() ):

    global $current_user;
    wp_get_current_user();
    $post_type_array = array('invoriginal', 'trabajoslibresrsc', 'trabajoslibresrio', 'mistrabajos');
    $author_query = array('post_type' => $post_type_array,'posts_per_page' => '-1','author' => $current_user->ID);
    $author_posts = new WP_Query($author_query);

    if($author_posts->have_posts()){
    ?>
        <ul>
    <?php
        while($author_posts->have_posts()) : $author_posts->the_post();
            $elid = get_post_type( $post->ID );
            $elide = get_post_type_object("$elid");
    ?>
            <li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>, inscrito en <a href="<?php echo $site_url; ?>/wp-admin/edit.php?post_type=<?php echo $elid; ?>"><?php echo $elide->labels->singular_name; ?></a>.</li>
        <?php           
        endwhile;
    ?>
        </ul>
    <?php
    } else {
        ?>
            <p>No tienes trabajos registrados, revisa las convocatorias e inscribe tu manuscrito.</p>
        <?php           
    }

else :

    echo "not logged in";

endif;
}





// REMOVE UNUSED PROFILE OPTIONS

add_action( 'admin_print_styles-profile.php', 'remove_profile_fields' );
add_action( 'admin_print_styles-user-edit.php', 'remove_profile_fields' );

function remove_profile_fields( $hook ) {
    ?>
    <style type="text/css">
        .user-rich-editing-wrap,
        .user-admin-color-wrap,
        .user-comment-shortcuts-wrap,
        .show-admin-bar,
        .user-language-wrap { display:none!important;visibility:hidden!important; }
    </style>
    <?php
} 

// CONTADOR METABOX

add_action( 'add_meta_boxes', 'add_events_metaboxes' );

$trabajosConContador = array('trabajoslibresrio', 'trabajoslibresrio');

function add_events_metaboxes() {
    add_meta_box('wpt_events_location', 'Contador de Caracteres', 'wpt_events_location', $trabajosConContador, 'side', 'default');
}

function wpt_events_location() {
    global $post;
    echo "El límite de caracteres para este trabajo es de <span id='amgcountermax'>3911</span> caracteres.";
    echo "<p id='amgcounter'>0000</p>";
    echo "<p id='amgcounteralert'>El trabajo no debe rebasar los caracteres marcados.</p>";

}

// Disable Post Content
// If is Estatus editorial != borrador, nuevo envío, regresado al autor
// If is Editor
// If is Revisor

// wp_enqueue_style( 'mysite-styles', get_stylesheet_directory_uri()."/css/disable.css" );


// ADD CUSTOM TEXT PUBLISH BUTTON 

add_action( 'post_submitbox_start', function() {
  $manuscritos_chiltheme_URL = get_stylesheet_directory_uri()."/tester.php";
    print "<a href='$manuscritos_chiltheme_URL'>Generate file</button><br>";
    $viewpost_url = get_permalink($post->ID);
    print "<a href='$viewpost_url'>Ver Trabajo</button><br>";
    // submit_button( 'Enviar Notificación', 'primary', 'enviar-notificacion' );
});

// CHANGE PUBLISH BUTTON TEXT

function change_publish_btn_txt() {
    echo "<script type='text/javascript'>jQuery(document).ready(function(){
        jQuery('#publish').attr('value', 'Your Custom Text');
    });</script>";
}
add_action('admin_footer-post-new.php', 'change_publish_btn_txt', 99);


// Guardar Mails en Custom Post Tyme Mensajes de Correo
// from: 
// to: 
// CC:
// BCC:
// subject:
// body: 

// add_filter('acf/update_value/name=trabajo_listo_para_revision', 'my_check_for_change_borrador', 10, 3);

// Guardar Mails

/**
  * Updates post meta for a post. It also automatically deletes or adds the value to field_name if specified
  *
  * @access     protected
  * @param      integer     The post ID for the post we're updating
  * @param      string      The field we're updating/adding/deleting
  * @param      string      [Optional] The value to update/add for field_name. If left blank, data will be deleted.
  * @return     void
  */

// public function __update_post_meta( $post_id, $field_name, $value = '' )
// {
//     if ( empty( $value ) OR ! $value )
//     {
//         delete_post_meta( $post_id, $field_name );
//     }
//     elseif ( ! get_post_meta( $post_id, $field_name ) )
//     {
//         add_post_meta( $post_id, $field_name, $value );
//     }
//     else
//     {
//         update_post_meta( $post_id, $field_name, $value );
//     }
// }

// $my_post = array(
//     'post_title' => $_SESSION['booking-form-title'],
//     'post_date' => $_SESSION['cal_startdate'],
//     'post_content' => 'This is my post.',
//     'post_status' => 'publish',
//     'post_type' => 'booking',
// );
// $the_post_id = wp_insert_post( $my_post );

// __update_post_meta( $the_post_id, 'my-custom-field', 'my_custom_field_value' );






/* --------------- */
// PROCESO EDITORIAL
/* --------------- */


// Si cambia un valor de los paneles de Editor, Revisor y Admin se actualiza el Proceso Editorial

// AUTOR trabajo listo

add_filter('acf/update_value/name=trabajo_listo_para_revision', 'my_check_for_change_borrador', 10, 3);
    
function my_check_for_change_borrador($value, $post_id, $field) {

  $old_value = get_post_meta($post_id,'trabajo_listo_para_revision');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'borrador' && $old_value[0] != $value && $value == 'Sí') {

        $manus_Title                              = get_the_title($post_id);

        $manus_EditorialStatus                    = get_post_custom_values( $key = "estatus_editorial" );
        $manus_SugerenciaPresentacion             = get_post_custom_values( $key = "sugerencia_de_presentacion" );
        $manus_Sugerencia                         = get_post_custom_values( $key = "sugerencia" );
        $manus_DecisionPresentacion               = get_post_custom_values( $key = "decision_de_presentacion" );
        $manus_Decision                           = get_post_custom_values( $key = "decision_final" );

        $manus_AuthorId                           = get_post_field( 'post_author', $post_id );
        $manus_AuthorEmail                        = get_the_author_meta( 'user_email', $manus_AuthorId );
        $manus_AuthorName                         = get_the_author_meta( 'first_name', $manus_AuthorId );

        $manus_Editor                             = get_post_custom_values( $key = "asignar_editor" );
        $manus_EditorName                         = get_the_author_meta( 'first_name', $manus_Editor[0] );
        $manus_EditorEmail                        = get_the_author_meta( 'user_email', $manus_Editor[0] );

        $manus_Revisor                            = get_post_custom_values( $key = "asignar_revisor" );
        $manus_RevisorName                        = get_the_author_meta( 'first_name', $manus_Revisor[0] );
        $manus_RevisorEmail                       = get_the_author_meta( 'user_email', $manus_Revisor[0] );

        $superAdminEmail = "juan.gonzalez@nuevaweb.com.mx";
        $adminEmail = "ltorres@gastro.org.mx";

        $correoCopia = array($manus_AuthorEmail, $superAdminEmail, $adminEmail);


        foreach ($correoCopia as $correoDestino) {
          $subject = 'AMG publicaciones: confirmación de envío';
          $to = $correoDestino;
          $body = "Estimado ". $manus_AuthorName . "<br><br>"
          . "Le confirmamos la recepción del artículo titulado: ". $manus_Title ." que nos ha enviado para su posible publicación en el Supl. 2 de la Revista de Gastroenterología de México." . "<br><br>"
          . "Usted podrá observar en el sistema el proceso de su resumen en el estatus editorial de su trabajo." . "<br><br>"
          . "En breve se iniciará el proceso de revisión de su resumen. En caso de que sea necesario que haga algún cambio, también se le notificará por correo electrónico." . "<br><br>"
          . "No dude en contactar con la redacción para cualquier información adicional." . "<br><br>"
          . "Reciba un cordial saludo," . "<br><br>"
          . "AMG" . "<br><br>"
          . "Revista de Gastroenterología de México" . "<br><br>"
          ;
          $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
          wp_mail( $to, $subject, $body, $headers );
        }

                update_field('estatus_editorial', 'nuevo envío', $post_id);
  }
  return $value;
}

// ADMIN asigna editor
add_filter('acf/update_value/name=asignar_editor', 'my_check_for_change_asignar_editor', 10, 3);
    
function my_check_for_change_asignar_editor($value, $post_id, $field) {



  $old_value = get_post_meta($post_id,'asignar_editor');
  $edo_editorial = get_field('estatus_editorial', $post_id);
    if ( $old_value[0] != $value ) {
      if ( $edo_editorial == 'revisión técnica' || $edo_editorial == 'nuevo envío' ) {

        $manus_Title                              = get_the_title($post_id);

        $manus_EditorialStatus                    = get_post_custom_values( $key = "estatus_editorial" );
        $manus_SugerenciaPresentacion             = get_post_custom_values( $key = "sugerencia_de_presentacion" );
        $manus_Sugerencia                         = get_post_custom_values( $key = "sugerencia" );
        $manus_DecisionPresentacion               = get_post_custom_values( $key = "decision_de_presentacion" );
        $manus_Decision                           = get_post_custom_values( $key = "decision_final" );

        $manus_AuthorId                           = get_post_field( 'post_author', $post_id );
        $manus_AuthorEmail                        = get_the_author_meta( 'user_email', $manus_AuthorId );
        $manus_AuthorName                         = get_the_author_meta( 'first_name', $manus_AuthorId );

        $manus_Editor                             = get_post_custom_values( $key = "asignar_editor" );
        $manus_EditorName                         = get_the_author_meta( 'first_name', $manus_Editor[0] );
        $manus_EditorEmail                        = get_the_author_meta( 'user_email', $manus_Editor[0] );

        $manus_Revisor                            = get_post_custom_values( $key = "asignar_revisor" );
        $manus_RevisorName                        = get_the_author_meta( 'first_name', $manus_Revisor[0] );
        $manus_RevisorEmail                       = get_the_author_meta( 'user_email', $manus_Revisor[0] );

        $superAdminEmail = "juan.gonzalez@nuevaweb.com.mx";
        $adminEmail = "ltorres@gastro.org.mx";

        $correoCopia = array($manus_EditorEmail, $superAdminEmail, $adminEmail);


        foreach ($correoCopia as $correoDestino) {
          $subject = 'Nuevo artículo asignado a Editor: ' . $manus_EditorName;
          $to = $correoDestino;
          $body = "Estimado ". $manus_EditorName . ", <br><br>"
          . "Te enviamos un artículo nuevo para que te hagas cargo de él. El título es: " . $manus_Title .". <br><br>"
          . "Puedes entrar a verlo directamente con el siguiente link <a href='https://amg-envio-manuscritos-cursos-resumenescongreso.mx/wp-admin/'>Ver Trabajos</a>" . "<br><br>"
          . "Muchas gracias, " . "<br><br>"
          . "Ma. de Lourdes Torres" . "<br>"
          . "Asistente editorial" . "<br>"
          . "Revista de Gastroenterología de México" . "<br>"
          ;
          $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
          wp_mail( $to, $subject, $body, $headers );
        }

        update_field('estatus_editorial', 'pendiente de editor', $post_id);

      }
    }
  return $value;
}

// EDITOR asigna revisor
add_filter('acf/update_value/name=asignar_revisor', 'my_check_for_change_asignar_revisor', 10, 3);
    
function my_check_for_change_asignar_revisor($value, $post_id, $field) {

  $old_value = get_post_meta($post_id,'asignar_revisor');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'pendiente de editor' && $old_value[0] != $value) {

        $manus_Title                              = get_the_title($post_id);

        $manus_EditorialStatus                    = get_post_custom_values( $key = "estatus_editorial" );
        $manus_SugerenciaPresentacion             = get_post_custom_values( $key = "sugerencia_de_presentacion" );
        $manus_Sugerencia                         = get_post_custom_values( $key = "sugerencia" );
        $manus_DecisionPresentacion               = get_post_custom_values( $key = "decision_de_presentacion" );
        $manus_Decision                           = get_post_custom_values( $key = "decision_final" );

        $manus_AuthorId                           = get_post_field( 'post_author', $post_id );
        $manus_AuthorEmail                        = get_the_author_meta( 'user_email', $manus_AuthorId );
        $manus_AuthorName                         = get_the_author_meta( 'first_name', $manus_AuthorId );

        $manus_Editor                             = get_post_custom_values( $key = "asignar_editor" );
        $manus_EditorName                         = get_the_author_meta( 'first_name', $manus_Editor[0] );
        $manus_EditorEmail                        = get_the_author_meta( 'user_email', $manus_Editor[0] );

        $manus_Revisor                            = get_post_custom_values( $key = "asignar_revisor" );
        $manus_RevisorName                        = get_the_author_meta( 'first_name', $manus_Revisor[0] );
        $manus_RevisorEmail                       = get_the_author_meta( 'user_email', $manus_Revisor[0] );

        $superAdminEmail = "juan.gonzalez@nuevaweb.com.mx";
        $adminEmail = "ltorres@gastro.org.mx";

        $correoCopia = array($manus_RevisorEmail, $superAdminEmail, $adminEmail);

        foreach ($correoCopia as $correoDestino) {
          $subject = 'AMG manuscritos: Recordatorio de revisión';
          $to = $correoDestino;
          $body = "Apreciado/a ". $manus_RevisorName . ", <br><br>"
          . "Hemos recibido el trabajo: " . $manus_Title . ",para su publicación en el Supl. 2 de la Revista de Gastroenterología de México. Le agradeceríamos que nos diera su calificación y recomendación si el trabajo amerita ser oral, cartel, aceptado y/o rechazado." .". <br><br>"
          . "Puedes entrar a verlo directamente con el siguiente link <a href='https://amg-envio-manuscritos-cursos-resumenescongreso.mx/wp-admin/'>Ver Trabajos</a>" . "<br><br>"
          . "Le recordamos su compromiso de realizar la revisión dentro del plazo establecido de una semana (7 días)" . "<br><br>"
          . "Este manuscrito es propiedad de los autores. Si consulta a otros revisores, por favor, proteja la privacidad del documento. Le agradecemos por anticipado su contribución al proceso editorial y su rápida evaluación, y quedamos a su disposición." . "<br><br>"
          . "Atentamente," . "<br>"
          . $manus_EditorName . "<br>"
          . "Editor Titular" . "<br>"
          . "Revista de Gastroenterología de México" . "<br>"
          ;
          $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
          wp_mail( $to, $subject, $body, $headers );
        }

        update_field('estatus_editorial', 'pendiente de revisor', $post_id);
  }
  return $value;
}
 
// REVISOR califica sugerencia
add_filter('acf/update_value/name=sugerencia', 'my_check_for_change_sugerencia', 10, 3);
    
function my_check_for_change_sugerencia($value, $post_id, $field) {


  $old_value = get_post_meta($post_id,'sugerencia');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'pendiente de revisor' && $old_value[0] != $value) {
        if ( $value != 'Pendiente' ){

          $manus_Title                              = get_the_title($post_id);

          $manus_EditorialStatus                    = get_post_custom_values( $key = "estatus_editorial" );
          $manus_SugerenciaPresentacion             = get_post_custom_values( $key = "sugerencia_de_presentacion" );
          $manus_Sugerencia                         = get_post_custom_values( $key = "sugerencia" );
          $manus_DecisionPresentacion               = get_post_custom_values( $key = "decision_de_presentacion" );
          $manus_Decision                           = get_post_custom_values( $key = "decision_final" );

          $manus_AuthorId                           = get_post_field( 'post_author', $post_id );
          $manus_AuthorEmail                        = get_the_author_meta( 'user_email', $manus_AuthorId );
          $manus_AuthorName                         = get_the_author_meta( 'first_name', $manus_AuthorId );

          $manus_Editor                             = get_post_custom_values( $key = "asignar_editor" );
          $manus_EditorName                         = get_the_author_meta( 'first_name', $manus_Editor[0] );
          $manus_EditorEmail                        = get_the_author_meta( 'user_email', $manus_Editor[0] );

          $manus_Revisor                            = get_post_custom_values( $key = "asignar_revisor" );
          $manus_RevisorName                        = get_the_author_meta( 'first_name', $manus_Revisor[0] );
          $manus_RevisorEmail                       = get_the_author_meta( 'user_email', $manus_Revisor[0] );

          $superAdminEmail = "juan.gonzalez@nuevaweb.com.mx";
          $adminEmail = "ltorres@gastro.org.mx";

          $correoCopia = array($manus_RevisorEmail, $superAdminEmail, $adminEmail);

          foreach ($correoCopia as $correoDestino) {
            $subject = 'Acuse de recibo de sus comentarios del trabajo:' . $manus_Title;
            $to = $correoDestino;
            $body = "Estimado/a ". $manus_RevisorName . ", <br><br>"
            . "Acusamos recibo de su calificación y recomendación del trabajo: " . $manus_Title . "<br><br>"
            . "Le recordamos que podrá consultar la decisión editorial sobre este manuscrito, en la columna de estatus editorial de su pantalla en el siguiente link: <a href='https://amg-envio-manuscritos-cursos-resumenescongreso.mx/wp-admin/'>Ver Trabajos</a>" . "<br><br>"
            . "Muchas gracias por el entusiasmo y apoyo brindado," . "<br><br>"
            . "Atentamente," . "<br>"
            . $manus_EditorName . "<br>"
            . "Editor Titular" . "<br>"
            . "Revista de Gastroenterología de México" . "<br>"
            ;
            $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
            wp_mail( $to, $subject, $body, $headers );
          }

          update_field('estatus_editorial', 'evaluaciones entregadas', $post_id);

        }
  }
  return $value;
}

// EDITOR toma decisión
add_filter('acf/update_value/name=decision_final', 'my_check_for_change_decision_final', 10, 3);

    
function my_check_for_change_decision_final($value, $post_id, $field) {

  $old_value = get_post_meta($post_id,'decision_final');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'evaluaciones entregadas' && $old_value[0] != $value) {
        if ( $value != 'Pendiente' ){

          $manus_Title                              = get_the_title($post_id);

          $manus_EditorialStatus                    = get_post_custom_values( $key = "estatus_editorial" );
          $manus_SugerenciaPresentacion             = get_post_custom_values( $key = "sugerencia_de_presentacion" );
          $manus_Sugerencia                         = get_post_custom_values( $key = "sugerencia" );
          $manus_DecisionPresentacion               = get_post_custom_values( $key = "decision_de_presentacion" );
          $manus_Decision                           = get_post_custom_values( $key = "decision_final" );

          $manus_AuthorId                           = get_post_field( 'post_author', $post_id );
          $manus_AuthorEmail                        = get_the_author_meta( 'user_email', $manus_AuthorId );
          $manus_AuthorName                         = get_the_author_meta( 'first_name', $manus_AuthorId );

          $manus_Editor                             = get_post_custom_values( $key = "asignar_editor" );
          $manus_EditorName                         = get_the_author_meta( 'first_name', $manus_Editor[0] );
          $manus_EditorEmail                        = get_the_author_meta( 'user_email', $manus_Editor[0] );

          $manus_Revisor                            = get_post_custom_values( $key = "asignar_revisor" );
          $manus_RevisorName                        = get_the_author_meta( 'first_name', $manus_Revisor[0] );
          $manus_RevisorEmail                       = get_the_author_meta( 'user_email', $manus_Revisor[0] );

          $superAdminEmail = "juan.gonzalez@nuevaweb.com.mx";
          $adminEmail = "ltorres@gastro.org.mx";

          $correoCopia = array($manus_EditorName, $superAdminEmail, $adminEmail);

          foreach ($correoCopia as $correoDestino) {
            $subject = 'Acuse de recibo decisión del trabajo:' . $manus_Title;
            $to = $correoDestino;
            $body = "Estimado/a ". $manus_EditorName . ", <br><br>"
            . "Acusamos recibo de decisión: " . $manus_EditorialStatus[0] . ", del trabajo: " . $manus_Title . ". <br><br>"
            . "Muchas gracias por el entusiasmo y apoyo brindado," . "<br><br>"
            . "Atentamente," . "<br>"
            . "Ma. de Lourdes Torres" . "<br>"
            . "Asistente Editorial" . "<br>"
            . "Revista de Gastroenterología de México" . "<br>"
            ;
            $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
            wp_mail( $to, $subject, $body, $headers );
          }

          update_field('estatus_editorial', 'decisión en curso', $post_id);
        }
  }
  return $value;
}

// ADMINISTRADOR ratifica
add_filter('acf/update_value/name=ratificar_trabajo', 'my_check_for_change_ratificar_trabajo', 10, 3);
    
function my_check_for_change_ratificar_trabajo($value, $post_id, $field) {





  $old_value = get_post_meta($post_id,'ratificar_trabajo');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'decisión en curso' && $old_value[0] != $value) {
        if ( $value == 'Sí' ){

          $manus_Title                              = get_the_title($post_id);

          $manus_EditorialStatus                    = get_post_custom_values( $key = "estatus_editorial" );
          $manus_SugerenciaPresentacion             = get_post_custom_values( $key = "sugerencia_de_presentacion" );
          $manus_Sugerencia                         = get_post_custom_values( $key = "sugerencia" );
          $manus_DecisionPresentacion               = get_post_custom_values( $key = "decision_de_presentacion" );
          $manus_Decision                           = get_post_custom_values( $key = "decision_final" );

          $manus_AuthorId                           = get_post_field( 'post_author', $post_id );
          $manus_AuthorEmail                        = get_the_author_meta( 'user_email', $manus_AuthorId );
          $manus_AuthorName                         = get_the_author_meta( 'first_name', $manus_AuthorId );

          $manus_Editor                             = get_post_custom_values( $key = "asignar_editor" );
          $manus_EditorName                         = get_the_author_meta( 'first_name', $manus_Editor[0] );
          $manus_EditorEmail                        = get_the_author_meta( 'user_email', $manus_Editor[0] );

          $manus_Revisor                            = get_post_custom_values( $key = "asignar_revisor" );
          $manus_RevisorName                        = get_the_author_meta( 'first_name', $manus_Revisor[0] );
          $manus_RevisorEmail                       = get_the_author_meta( 'user_email', $manus_Revisor[0] );

          $superAdminEmail = "juan.gonzalez@nuevaweb.com.mx";
          $adminEmail = "ltorres@gastro.org.mx";

          $correoCopia = array($manus_AuthorName, $superAdminEmail, $adminEmail);

          foreach ($correoCopia as $correoDestino) {
            $subject = 'Decisión de los editores del trabajo: ' . $manus_Title;
            $to = $correoDestino;
            $body = "Estimado/a ". $manus_AuthorName . ", <br><br>"
            . "Le comunicamos que su manuscrito: " . $manus_Title . ", ha sido: <b>" . $manus_EditorialStatus[0] . "</b><br><br>"
            . "Atentamente," . "<br>"
            . "Ma. de Lourdes Torres" . "<br>"
            . "Asistente Editorial" . "<br>"
            . "Revista de Gastroenterología de México" . "<br>"
            ;
            $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
            wp_mail( $to, $subject, $body, $headers );
          }
          

          update_field('estatus_editorial', 'aceptado', $post_id);
        }
  }
  return $value;
}







add_action( 'admin_menu', 'mgi_add_admin_menu' );
add_action( 'admin_init', 'mgi_settings_init' );


function mgi_add_admin_menu(  ) { 

  add_options_page( 'Margaritos', 'Margaritos', 'manage_options', 'margaritos', 'mgi_options_page' );

}


function mgi_settings_init(  ) { 

  register_setting( 'pluginPage', 'mgi_settings' );

  add_settings_section(
    'mgi_pluginPage_section', 
    __( 'Your section description', 'Margarito' ), 
    'mgi_settings_section_callback', 
    'pluginPage'
  );

  add_settings_field( 
    'mgi_text_field_0', 
    __( 'Settings field description', 'Margarito' ), 
    'mgi_text_field_0_render', 
    'pluginPage', 
    'mgi_pluginPage_section' 
  );

  add_settings_field( 
    'mgi_select_field_1', 
    __( 'Settings field description', 'Margarito' ), 
    'mgi_select_field_1_render', 
    'pluginPage', 
    'mgi_pluginPage_section' 
  );


}


function mgi_text_field_0_render(  ) { 

  $options = get_option( 'mgi_settings' );
  ?>
  <input type='text' name='mgi_settings[mgi_text_field_0]' value='<?php echo $options['mgi_text_field_0']; ?>'>
  <?php

}


function mgi_select_field_1_render(  ) { 

  $options = get_option( 'mgi_settings' );
  ?>
  <select name='mgi_settings[mgi_select_field_1]'>
    <option value='1' <?php selected( $options['mgi_select_field_1'], 1 ); ?>>Option 1</option>
    <option value='2' <?php selected( $options['mgi_select_field_1'], 2 ); ?>>Option 2</option>
  </select>

<?php

}


function mgi_settings_section_callback(  ) { 

  echo __( 'This section description', 'Margarito' );

}


function mgi_options_page(  ) { 

  ?>
  <form action='options.php' method='post'>

    <h2>Margaritos</h2>

    <?php
    settings_fields( 'pluginPage' );
    do_settings_sections( 'pluginPage' );
    submit_button();
    ?>

  </form>
  <?php

}



// TITULO de POSTS OBLIGATORIO

// Load admin scripts & styles
function km_load_admin_scripts( $hook ) {
  global $post;
  
  wp_enqueue_script( 'admin_scripts', get_stylesheet_directory_uri() . '/js/km_dashboard_admin.js', array( 'jquery' ) );
  // if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
  // }
}
add_action( 'admin_enqueue_scripts', 'km_load_admin_scripts' );



// CAMBIAR EDIT PROFILE

add_action( 'wp_before_admin_bar_render', 'my_tweaked_admin_bar' ); 

function my_tweaked_admin_bar() {
    global $wp_admin_bar;

    $wp_admin_bar->remove_menu('edit-profile');

    $wp_admin_bar->add_menu( array(
            'id'    => 'edit-profile',
            'title' => 'Editar Perfil',
            'href'  => admin_url('profile.php'),
            'parent'=>'user-actions'
        ));
}




// USER ROLE SWITCHING

// this function sets a new user meta key to the value of thier current roles, enabling them to switch between roles
add_action('after_switch_theme', 'set_roles_in_switch_user_function');

function set_roles_in_switch_user_function($wp_admin_bar){

  $all_users = get_users();
  $switch_roles = array('editor', 'author', 'revisor');

  foreach ($all_users as $user) {
    $roles = array();

    foreach ($switch_roles as $switch_role) {
      if (in_array($switch_role, $user->roles)) array_push($roles, $switch_role);
    }

    if (count($roles)) update_user_meta($user->id, 'switch_user_function_roles', $roles);
  }

}

// as above, but does on profile update instead of theme switch
add_action('edit_user_profile_update', 'update_role_in_switch_user_function', 998, 3);

function update_role_in_switch_user_function($user_id, $role, $old_roles){

if(!isset($_POST['md_multiple_roles'])) return;

  $user = get_userdata($user_id);
  $switch_roles = array('editor', 'author', 'revisor');
  $roles = array();
  foreach ($switch_roles as $switch_role) {
    if (in_array($switch_role, $_POST['md_multiple_roles'])) array_push($roles, $switch_role);
  }

  if (count($roles)) update_user_meta($user_id, 'switch_user_function_roles', $roles);
}

add_action('edit_user_profile', 'admin_view_of_roles', 1, 1);

function admin_view_of_roles($user){

  $current_roles = get_user_meta($user->id, 'switch_user_function_roles', true);

  foreach ($current_roles as $role) {
    $user->add_role($role);
  }
}

add_action('admin_bar_menu', 'customise_admin_toolbar', 999);

function customise_admin_toolbar($wp_admin_bar){

  $user = wp_get_current_user();

  if (!($user instanceof WP_User)){
    return;
  }

  $user_roles = get_user_meta($user->id, 'switch_user_function_roles', true);

  $my_account = $wp_admin_bar->get_node('my-account');

  foreach ($user_roles as $role) {
    if ($role == 'revisor' || $role == 'editor' || $role == 'author') {
      if($my_account){
        $wp_admin_bar->add_node(array(
          'parent'    => 'user-actions',
          'id'    => 'change-user-' . $role,
          'title'   => '<span>' . 'Cambiar a ' . ucfirst($role) . '</span>',
          'href'    => get_stylesheet_directory_uri() . '/modules/switch-roles.php?role=' . $role
          ) );
      }
    }
  }
}