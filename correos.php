<?php

function my_dashboard_mymail_widget_display() {
    $to = 'ltorres@gastro.org.mx';
    $subject = 'Correo de Aceptado';
    $body = 'Su trabajo ha sido aceptado';
    $headers = array('Content-Type: text/html; charset=UTF-8','From: My Site Name <support@example.com');
    wp_mail( $to, $subject, $body, $headers );
}

$queconvocatoria = "";
$queestatus = "";
$querol = "";
$quemensaje = "";


$amg_mergetags = array(
		"titulo" = $postTitle;
		"antecedentes" = $postAcfAntecedentes;
		"objetivos" = $postAcfObjetivos;
		"id" = $postId;
	);

$amg_maildir = array(
		"ECOS" => array(
			"NuevoEnvio" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RevisionTecnica" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RetiradoPorDuplicado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DevueltoAlAutor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeRevisor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"EvaluacionesEntregadas" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionDelEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionEnCurso" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rechazado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rceptado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);
			);,
		"TLRSC" => array(
			"NuevoEnvio" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RevisionTecnica" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RetiradoPorDuplicado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DevueltoAlAutor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeRevisor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"EvaluacionesEntregadas" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionDelEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionEnCurso" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rechazado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rceptado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);
			);,
		"TLIO" => array(
			"NuevoEnvio" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RevisionTecnica" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RetiradoPorDuplicado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DevueltoAlAutor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeRevisor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"EvaluacionesEntregadas" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionDelEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionEnCurso" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rechazado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rceptado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);
			);,
		"IO" => array(
			"NuevoEnvio" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RevisionTecnica" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RetiradoPorDuplicado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DevueltoAlAutor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeRevisor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"EvaluacionesEntregadas" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionDelEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionEnCurso" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rechazado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rceptado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);
			);,
		"ER" => array(
			"NuevoEnvio" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RevisionTecnica" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"RetiradoPorDuplicado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DevueltoAlAutor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"PendienteDeRevisor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"EvaluacionesEntregadas" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionDelEditor" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"DecisionEnCurso" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rechazado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);,
			"Rceptado" => array(
					"autor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"revisor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"editor" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
					"administrador" => array(
							"to" => $userMail,
							"subject" => "asunto",
							"body" => "Texto"
						);
				);
			);,
	);


?>