<?php

function jmc_theme_options_panel(){
    // add_menu_page('Theme page title', 'JMC Dashboard', 'manage_options', 'jmc-dashboard-options', 'manuscritos_jmc_dashboard');
    add_menu_page('Theme page title', 'JMC Dashboard', 'manage_options', 'jmc-dashboard-options', 'test_plugin_admin_options');
}

add_action('admin_menu', 'jmc_theme_options_panel');

function manuscritos_jmc_dashboard(){
    echo "holacrayola";
}

add_action( 'admin_init', 'register_test_plugin_settings' );
function register_test_plugin_settings() {
    //register our settings
    register_setting( 'test-plugin-settings-group', 'new_option_name' );
    register_setting( 'test-plugin-settings-group', 'some_other_option' );
}

//create page content and options
function test_plugin_admin_options(){
?>
    <h1>Test Plugin</h1>
    <form method="post" action="#">
        <?php settings_fields( 'test-plugin-settings-group' ); ?>
        <?php do_settings_sections( 'test-plugin-settings-group' ); ?>
        <table class="form-table">
          <tr valign="top">
          <th scope="row">New Option 1:</th>
          <td><input type="text" name="new_option_name" value="<?php echo get_option( 'new_option_name' ); ?>"/></td>
          </tr>
          <tr valign="top">
          <th scope="row">New Option 2:</th>
          <td><input type="text" name="some_other_option" value="<?php echo get_option( 'some_other_option' ); ?>"/></td>
          </tr>
        </table>
    <?php submit_button(); ?>
    </form>
<?php

  }

?>