<?php

// - - - - - - - - - - - - - - - - - - - - - - - - - - -
// CALIFICACIONES METABOX
// - - - - - - - - - - - - - - - - - - - - - - - - - - -

function custom_meta_box_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
        <div>
            <p>Califique del 1 al 5 de acuerdo a los siguientes criterios de evaluación:</p>
            <label for="meta-box-dropdown-one"><b>1.- Originalidad:</b> ¿El manuscrito presenta datos nuevos?</label>
            <br><br>

            <select name="meta-box-dropdown-one">
                <?php 
                    $option_values = array(1, 2, 3, 4, 5);

                    foreach($option_values as $key => $value) 
                    {
                        if($value == get_post_meta($object->ID, "meta-box-dropdown-one", true))
                        {
                            ?>
                                <option selected><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>
            <br><br>

            <label for="meta-box-dropdown-two"><b>2.-Métodos y Diseño:</b> ¿Son apropiados para responder las preguntas, hipótesis u objetivos planteados?</label>
            <br><br>

            <select name="meta-box-dropdown-two">
                <?php 
                    $option_values = array(1, 2, 3, 4, 5);

                    foreach($option_values as $key => $value) 
                    {
                        if($value == get_post_meta($object->ID, "meta-box-dropdown-two", true))
                        {
                            ?>
                                <option selected><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>
            <br><br>

            <label for="meta-box-dropdown-one-three"><b>3. Conclusiones:</b> ¿Las conclusiones son apoyadas por los resultados del estudio?</label>
            <br><br>

            <select name="meta-box-dropdown-three">
                <?php 
                    $option_values = array(1, 2, 3, 4, 5);

                    foreach($option_values as $key => $value) 
                    {
                        if($value == get_post_meta($object->ID, "meta-box-dropdown-three", true))
                        {
                            ?>
                                <option selected><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>
            <br><br>

            <label for="meta-box-dropdown-four"><b>4.- Impacto clínico, y actualidad:</b> a) la información tiene o podría modificar conceptos, manejo actuales? b) Los resultados son generadores de hipótesis o justifican la realización de estudios clínicos? c) La relevancia de los resultados se menciona claramente? d) El tema y resultados son de interés actual?</label>
            <br><br>

            <select name="meta-box-dropdown-four">
                <?php 
                    $option_values = array(1, 2, 3, 4, 5);

                    foreach($option_values as $key => $value) 
                    {
                        if($value == get_post_meta($object->ID, "meta-box-dropdown-four", true))
                        {
                            ?>
                                <option selected><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>
            <br><br>

            <label for="meta-box-dropdown-five"><b>5.- Presentación:</b> a) ¿El resumen es claro y fácil de entender? b) ¿Se comprende fácilmente el objetivo, resultados y conclusiones</label>
            <br><br>

            <select name="meta-box-dropdown-five">
                <?php 
                    $option_values = array(1, 2, 3, 4, 5);

                    foreach($option_values as $key => $value) 
                    {
                        if($value == get_post_meta($object->ID, "meta-box-dropdown-five", true))
                        {
                            ?>
                                <option selected><?php echo $value; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option><?php echo $value; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>
            
            <br><br><br>

            <label for="meta-box-comment">Comentarios</label><br><br>
            <textarea name="meta-box-comment"><?php echo get_post_meta($object->ID, "meta-box-comment", true); ?></textarea>

            <br><br><br>

            <label for="meta-box-reject">Rechazar</label>
            <p>Seleccione esta casila si el trabajo ha sido rechazado.</p>
            <?php
                $checkbox_value = get_post_meta($object->ID, "meta-box-reject", true);

                if($checkbox_value == "")
                {
                    ?>
                        <input name="meta-box-reject" type="checkbox" value="true">
                    <?php
                }
                else if($checkbox_value == "true")
                {
                    ?>  
                        <input name="meta-box-reject" type="checkbox" value="true" checked>
                    <?php
                }
            ?>
        </div>
    <?php  
}

 function save_custom_meta_box($score_post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $score_post_id;

    if(!current_user_can("edit_post", $score_post_id))
        return $score_post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $score_post_id;

    $slug = "trabajoslibresrio";

    if($slug != $post->post_type)
        return $score_post_id;

    $meta_box_dropdown_one_value = "";
    $meta_box_dropdown_two_value = "";
    $meta_box_dropdown_three_value = "";
    $meta_box_dropdown_four_value = "";
    $meta_box_dropdown_five_value = "";
    $meta_box_comment_value = "";
    $meta_box_reject_value = "";

    if(isset($_POST["meta-box-dropdown-one"]))
    {
        $meta_box_dropdown_one_value = $_POST["meta-box-dropdown-one"];
    }   
    update_post_meta($score_post_id, "meta-box-dropdown-one", $meta_box_dropdown_one_value);

    if(isset($_POST["meta-box-dropdown-two"]))
    {
        $meta_box_dropdown_two_value = $_POST["meta-box-dropdown-two"];
    }   
    update_post_meta($score_post_id, "meta-box-dropdown-two", $meta_box_dropdown_two_value);

    if(isset($_POST["meta-box-dropdown-three"]))
    {
        $meta_box_dropdown_three_value = $_POST["meta-box-dropdown-three"];
    }   
    update_post_meta($score_post_id, "meta-box-dropdown-three", $meta_box_dropdown_three_value);

    if(isset($_POST["meta-box-dropdown-four"]))
    {
        $meta_box_dropdown_four_value = $_POST["meta-box-dropdown-four"];
    }   
    update_post_meta($score_post_id, "meta-box-dropdown-four", $meta_box_dropdown_four_value);

    if(isset($_POST["meta-box-dropdown-five"]))
    {
        $meta_box_dropdown_five_value = $_POST["meta-box-dropdown-five"];
    }   
    update_post_meta($score_post_id, "meta-box-dropdown-five", $meta_box_dropdown_five_value);

    if(isset($_POST["meta-box-comment"]))
    {
        $meta_box_comment_value = $_POST["meta-box-comment"];
    }   
    update_post_meta($score_post_id, "meta-box-comment", $meta_box_comment_value);

    if(isset($_POST["meta-box-reject"]))
    {
        $meta_box_reject_value = $_POST["meta-box-reject"];
    }   
    update_post_meta($score_post_id, "meta-box-reject", $meta_box_reject_value);
}

add_action("save_post", "save_custom_meta_box", 10, 3);

function add_custom_calificaciones_meta_box()
{
    add_meta_box("calificaciones-meta-box", "Calificaciones", "custom_meta_box_markup", "trabajoslibresrio", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_calificaciones_meta_box");

?>