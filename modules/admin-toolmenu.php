<?php

// - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ADD ADMIN MENU
// - - - - - - - - - - - - - - - - - - - - - - - - - - -

/*
    add a group of links under a parent link
*/

// Add a parent shortcut link

function custom_toolbar_link($wp_admin_bar) {
    $args = array(
        'id' => 'registrar-trabajo',
        'title' => 'Registrar Trabajo', 
        'href' => 'http://amg-envio-manuscritos-cursos-resumenescongreso.mx/', 
        'meta' => array(
            'class' => 'registrar-trabajo', 
            'title' => 'registrar-trabajo'
            )
    );
    $wp_admin_bar->add_node($args);

    // Add the first child link 
    
    $args = array(
        'id' => 'trabajos-libres-resumen-de-investigacion-original',
        'title' => 'trabajos-libres-resumen-de-investigacion-original', 
        'href' => 'http://amg-envio-manuscritos-cursos-resumenescongreso.mx/trabajos-libres-resumen-de-investigacion-original/',
        'parent' => 'registrar-trabajo', 
        'meta' => array(
            'class' => 'trabajos-libres-resumen-de-investigacion-original', 
            'title' => 'trabajos-libres-resumen-de-investigacion-original'
            )
    );
    $wp_admin_bar->add_node($args);

    // Add another child link

    $args = array(
            'id' => 'trabajos-libres-resumen-de-series-de-casos',
            'title' => 'trabajos-libres-resumen-de-series-de-casos', 
            'href' => 'http://amg-envio-manuscritos-cursos-resumenescongreso.mx/trabajos-libres-resumen-de-series-de-casos/',
            'parent' => 'registrar-trabajo', 
            'meta' => array(
                'class' => 'trabajos-libres-resumen-de-series-de-casos', 
                'title' => 'trabajos-libres-resumen-de-series-de-casos'
                )
        );
        $wp_admin_bar->add_node($args);

    // Add another child link
    
    $args = array(
            'id' => 'ecos-internacionales',
            'title' => 'ecos-internacionales', 
            'href' => 'http://amg-envio-manuscritos-cursos-resumenescongreso.mx/ecos-internacionales/',
            'parent' => 'registrar-trabajo', 
            'meta' => array(
                'class' => 'ecos-internacionales', 
                'title' => 'ecos-internacionales'
                )
        );
        $wp_admin_bar->add_node($args);

    // Add another child link
    
    $args = array(
            'id' => 'investigacion-original-para-concurso-en-extenso',
            'title' => 'investigacion-original-para-concurso-en-extenso', 
            'href' => 'http://amg-envio-manuscritos-cursos-resumenescongreso.mx/investigacion-original-para-concurso-en-extenso/',
            'parent' => 'registrar-trabajo', 
            'meta' => array(
                'class' => 'investigacion-original-para-concurso-en-extenso', 
                'title' => 'investigacion-original-para-concurso-en-extenso'
                )
        );
        $wp_admin_bar->add_node($args);

    // Add another child link
    
    $args = array(
            'id' => 'viii-encuentro-de-residentes-exclusivo-socios-amg',
            'title' => 'viii-encuentro-de-residentes-exclusivo-socios-amg', 
            'href' => 'http://amg-envio-manuscritos-cursos-resumenescongreso.mx/viii-encuentro-de-residentes-exclusivo-socios-amg/',
            'parent' => 'registrar-trabajo', 
            'meta' => array(
                'class' => 'viii-encuentro-de-residentes-exclusivo-socios-amg', 
                'title' => 'viii-encuentro-de-residentes-exclusivo-socios-amg'
                )
        );
        $wp_admin_bar->add_node($args);

}

add_action('admin_bar_menu', 'custom_toolbar_link', 999);

?>