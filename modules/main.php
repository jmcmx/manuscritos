<?php

namespace Manuscritos\Modules;

class Main
{

    public static function init()
    {
        // Estilos
        add_action( 'wp_enqueue_scripts', array(__CLASS__, 'enqueue_scripts'), 9999 );
        // Agregar revisor
        add_action( 'after_switch_theme', array(__CLASS__, 'add_roles'));
        // Unused
        add_action( 'add_meta_boxes', array(__CLASS__, 'add_meta_boxes'));
        add_filter( 'pre_get_posts', array(__CLASS__, 'trabajos_for_current_author'));
        add_filter( 'pre_get_posts', array(__CLASS__, 'trabajos_for_current_author_role'));
        // Unused
        add_action( 'save_post', array(__CLASS__, 'save_post_data' ) );
        // Unused
        add_action( 'admin_enqueue_scripts', array(__CLASS__, 'disable_post_en_revision'));
    }

    public static function enqueue_scripts()
    {
        wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    }

    public static function add_roles () {

        remove_role('revisor');

        $new_roles = array
        (
            array('revisor', 'Revisor', array('read'=> true, 'edit_posts'=> true, 'edit_others_posts'=> true, 'edit_published_posts'=> true))
            );

        foreach ($new_roles as $role) {

            $result = add_role($role[0], $role[1], $role[2]);
        }
    }

    public static function add_meta_boxes () {

        $all_revisores = self::get_users_with_role('revisor');
        $all_editors = self::get_users_with_role('editor');

        $statuses = array(
            array('ID'  =>  'sin-revision', 'name'  => 'Sin Revisión'),
            array('ID'  =>  'entregado-editores', 'name'  => 'Entregado a Editores'),
            array('ID'  =>  'en-revision', 'name'  => 'En Revisión'),
            array('ID'  =>  'revision-menor', 'name'  => 'Revisión Menor'),
            array('ID'  =>  'revision-mayor', 'name'  => 'Revisión Mayor'),
            array('ID'  =>  'revisado', 'name'  => 'Revisado'),
            array('ID'  =>  'rechazado', 'name'  => 'Rechazado'),
            array('ID'  =>  'aprobado', 'name'  => 'Aprobado')
        );


         $metaboxes = array
         (  
            array(
                'id' => 'editor',
                'title' => 'Editor',
                'page' => 'mistrabajos',
                'context' => 'side',
                'priority' => 'high',
                'fields' => array(
                    'id' => 'editor',
                    'options' => $all_editors
                    )
                ),
            array(
                'id' => 'revisor1',
                'title' => 'Revisor Asignado 1',
                'page' => 'mistrabajos',
                'context' => 'side',
                'priority' => 'high',
                'fields' => array(
                    'id' => 'revisor-select-1',
                    'options' => $all_revisores
                    )
                ),
            array(
                'id' => 'revisor2',
                'title' => 'Revisor Asignado 2',
                'page' => 'mistrabajos',
                'context' => 'side',
                'priority' => 'high',
                'fields' => array(
                    'id' => 'revisor-select-2',
                    'options' => $all_revisores
                    )
                ),
            array(
                'id' => 'trabajo_status',
                'title' => 'Status de Trabajo',
                'page' => 'mistrabajos',
                'context' => 'side',
                'priority' => 'high',
                'fields' => array(
                    'id' => 'trabajo-status-select',
                    'options' => $statuses
                    )
                )
            );

         foreach ($metaboxes as $meta_box) {

            add_meta_box($meta_box['id'], $meta_box['title'], array(__CLASS__, 'add_select_box'), $meta_box['page'], $meta_box['context'], $meta_box['priority'], $meta_box['fields']);
        }
    }

    public static function get_users_with_role ($role) {
        $args = array(
            'role'         => $role,
            'orderby'      => 'display_name',
            'order'        => 'ASC',
            'fields'       => array('ID', 'display_name') 
            ); 
        $all_users_query = get_users( $args );
        $all_users = array();
        array_push($all_users, array('ID'  =>  0, 'name'  =>  '--'));

        foreach ($all_users_query as $value) {
           array_push($all_users, array(
            'ID'  =>  $value->ID,
            'name'  =>  $value->display_name,
            )
           );
       }
       return $all_users;
    }

    public static function add_select_box( $post, $fields ) {

        wp_nonce_field( 'add_custom_metabox', 'post_' .  $post->ID ); 

        $args = $fields['args'];

        $values = get_post_custom( $post->ID );
        $selected = isset( $values[$args['id']] ) ? esc_attr( $values[$args['id']][0] ) : '';

        ?>

        <p>
            <br />
            <select name="<?php echo $args['id']?>"
                id="<?php echo $args['id']?>"
                style="width:100%">

                <?php foreach($args['options'] as $status): ?>

                    <option value="<?php echo $status['ID'] ?>" <?php selected( $selected, $status['ID'] ); ?>><?php echo $status['name'] ?></option>

                <?php endforeach ?>
            </select>
        </p>
        <?php
    }

    public static function trabajos_for_current_author( $wp_query ) {
        if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/edit.php' ) !== false && $wp_query->is_main_query()) {
            global $current_user;

            if ( !current_user_can( 'moderate_comments' ))  { // if less than editor
                add_action( 'views_edit-mistrabajos', array(__CLASS__, 'child_remove_some_post_views') );
                
                if ( current_user_can( 'publish_posts' ))  { // is author
                    $wp_query->set('author' , $current_user->ID);
                }
                else { // is revisor
                    $wp_query->set('meta_query', array(
                        'relation' => 'OR',
                        array(
                            'key'     => 'revisor-select-1',
                            'value'   => $current_user->ID
                            ),
                        array(
                            'key'     => 'revisor-select-2',
                            'value'   => $current_user->ID
                            )
                        )
                    );
                }
            }
        }
    }

    public static function trabajos_for_current_author_role( $wp_query ) {
        if ( $wp_query->is_main_query()) {
            global $current_user;
            $user_meta=get_userdata($current_user->ID);
            $user_roles=$user_meta->roles;

                // var_dump($user_roles[0]);
                // echo $current_user;
            if ( $user_roles[0] == 'author'){
                $wp_query->set('author' , $current_user->ID);
            } elseif ( current_user_can( 'edit_themes' ) ){
                return $wp_query;
            } elseif ( current_user_can( 'moderate_comments' ) || current_user_can( 'edit_posts' ) )  { // if less than editor
                $wp_query->set('meta_query',array(
                        'relation' => 'OR',
                        array(
                            'key'     => 'asignar_editor',
                            'value'   => $current_user->ID
                            ),
                        array(
                            'key'     => 'asignar_revisor',
                            'value'   => $current_user->ID
                            )
                        ));
            }   


        }
        return $wp_query;
    }


    public static function child_remove_some_post_views( $views ) {
        unset($views['all']);
        unset($views['publish']);
        unset($views['trash']);
        unset($views['draft']);
        unset($views['pending']);
        return $views;
    }

    public static function save_post_data( $post_id )
    {

            // Check if our nonce is set.
        if ( ! isset( $_POST['post_' . $post_id] ) ) return $post_id;

        $nonce = $_POST['post_' . $post_id];

            // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'add_custom_metabox' ) ) return $post_id;

            // If this is an autosave, our form has not been submitted so we don't want to do anything
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;

            // Check the user's permissions.
        if ( ! current_user_can( 'edit_post', $post_id ) ) return $post_id;

        if( isset( $_POST['editor'] ) )
            update_post_meta( $post_id, 'editor', esc_attr( $_POST['editor'] ) );
        if( isset( $_POST['revisor-select-1'] ) )
            update_post_meta( $post_id, 'revisor-select-1', esc_attr( $_POST['revisor-select-1'] ) );
        if( isset( $_POST['revisor-select-2'] ) )
            update_post_meta( $post_id, 'revisor-select-2', esc_attr( $_POST['revisor-select-2'] ) );
        if( isset( $_POST['trabajo-status-select'] ) )
            update_post_meta( $post_id, 'trabajo-status-select', esc_attr( $_POST['trabajo-status-select'] ) );
    }

    public static function disable_post_en_revision () {

        if (get_post_type() != 'mistrabajos') return;

        $current_user = wp_get_current_user();
        $roles = $current_user->roles;
        if(array_shift( $roles ) != 'author') return;

        $value = get_post_meta( get_the_ID(), 'trabajo-status-select', true);
        if ($value != 'en-revision') return;

        add_filter( 'tiny_mce_before_init', function( $args ) {
         $args['readonly'] = 1;
         return $args;
        } );
        wp_enqueue_script('admin_title_disable', '/wp-content/themes/manuscritos/js/main.js');
    }
}