<?php

// CAMBIAR TEXTOS DE PUBLISH Y PREVIEW

/*
Plugin Name: Retranslate
Description: Adds translations.
Version:     0.1
Author:      Thomas Scholz
Author URI:  http://toscho.de
License:     GPL v2
*/

class Toscho_Retrans {
    // store the options
    protected $params;

    /**
     * Set up basic information
     * 
     * @param  array $options
     * @return void
     */
    public function __construct( array $options )
    {
        $defaults = array (
            'domain'       => 'default'
        ,   'context'      => 'backend'
        ,   'replacements' => array ()
        ,   'post_type'    => array ( 'post' )
        );

        $this->params = array_merge( $defaults, $options );

        // When to add the filter
        $hook = 'backend' == $this->params['context'] 
            ? 'admin_head' : 'template_redirect';

        add_action( $hook, array ( $this, 'register_filter' ) );
    }

    /**
     * Conatiner for add_filter()
     * @return void
     */
    public function register_filter()
    {
        add_filter( 'gettext', array ( $this, 'translate' ), 10, 3 );
    }

    /**
     * The real working code.
     * 
     * @param  string $translated
     * @param  string $original
     * @param  string $domain
     * @return string
     */
    public function translate( $translated, $original, $domain )
    {
        if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/edit.php' ) !== false ) {
            // exit early
            if ( 'backend' == $this->params['context'] )
            {
                global $post_type;

                if ( ! empty ( $post_type ) 
                    && ! in_array( $post_type, $this->params['post_type'] ) )
                {
                    return $translated;
                }
            }

            if ( $this->params['domain'] !== $domain )
            {
                return $translated;
            }

            // Finally replace
            return strtr( $original, $this->params['replacements'] );
        }
            return strtr( $original, $this->params['replacements'] );
    }
}

// Sample code
// Replace 'Publish' with 'Save' and 'Preview' with 'Lurk' on pages and posts
$Toscho_Retrans = new Toscho_Retrans(
    array (
        'replacements' => array ( 
            'Publish' => 'Guardar'
        ,   'Update' => 'Guardar Cambios' 
        ,   'Preview' => 'Vista Previa' 
        ,   'Edit' => 'Editar' 
        ,   'View' => 'Ver' 
        ,   'Draft' => 'Guardar y seguir editando' 
        )
    ,   'post_type'    => array ( 'invoriginal', 'trabajoslibresrsc', 'trabajoslibresrio', 'mistrabajos' )
    )
);

?>