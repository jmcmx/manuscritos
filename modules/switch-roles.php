<?php

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require_once __DIR__ . '/../../../../wp-load.php';

$user = wp_get_current_user();

if (!($user instanceof WP_User)){
	return;
}

$role = $_GET['role'];
$current_roles = get_user_meta($user->id, 'switch_user_function_roles', true);

if (!in_array($role, $current_roles)) {
	echo '<h1 style="color:red">Este role no est� asignado a tu perfil</h1>';
	return;
}
if ($role == 'editor') {
	$user->remove_role( 'revisor' );
	$user->remove_role( 'author' );
	$user->add_role( 'editor' );
}
elseif ($role == 'author') {
	$user->remove_role( 'revisor' );
	$user->remove_role( 'editor' );
	$user->add_role( 'author' );
}
elseif ($role == 'revisor') {
	$user->remove_role( 'editor' );
	$user->remove_role( 'author' );
	$user->add_role( 'revisor' );
}

wp_redirect(site_url() . '/wp-admin');

exit;