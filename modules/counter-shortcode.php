<?php

// SHORTCODE CONTADOR

function counterConfig( $atts ) {
  
  $myurl = get_stylesheet_directory_uri();

  $manusCounterForm = $atts['form'];

  switch ($manusCounterForm) {
    case 1:
        wp_enqueue_script('script-typed', $myurl."/js/contador.js", array('jquery'));
        break;
    case 2:
        wp_enqueue_script('script-typed', $myurl."/js/contador2.js", array('jquery'));
        break;
    case 3:
        wp_enqueue_script('script-typed', $myurl."/js/contador3.js", array('jquery'));
        break;
    case 4:
        wp_enqueue_script('script-typed', $myurl."/js/contador4.js", array('jquery'));
        break;
    case 5:
        wp_enqueue_script('script-typed', $myurl."/js/contador5.js", array('jquery'));
        break;
    default:
        wp_enqueue_script('script-typed', $myurl."/js/contador.js", array('jquery'));
  }

  return $counterInfo;
  
  wp_add_inline_script ('script-typed', $script_to_add);
  $script_to_add = "jQuery(function(){console.log('Im an inline Code');});";


}

function counterText( $atts ) {
    $output = '';
    $output.= "<div id='contador' style='color: red;'>Contador</div>";
    return $output;
}

function register_shortcodes(){
	add_shortcode('counter', 'counterConfig');
	add_shortcode('counter-text', 'counterText');
}

add_action( 'init', 'register_shortcodes');

// Remove dashboard widgets
function remove_dashboard_meta() {
  if ( ! current_user_can( 'manage_options' ) ) {
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
  }
}
add_action( 'admin_init', 'remove_dashboard_meta' ); 

?>