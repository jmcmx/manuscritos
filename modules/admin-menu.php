<?php

function theme_options_panel(){
  add_menu_page('Theme page title', 'Usuarios', 'manage_options', 'theme-options', 'manus_user_func');
  add_submenu_page( 'theme-options', 'Editores page title', 'Editores', 'manage_options', 'user-sh-editor', 'manus_user_func_editor');
  add_submenu_page( 'theme-options', 'Revisores page title', 'Revisores', 'manage_options', 'user-sh-revisor', 'manus_user_func_revisor');
  add_submenu_page( 'theme-options', 'Autores page title', 'Autores', 'manage_options', 'user-sh-autor', 'manus_user_func_autor');
}

add_action('admin_menu', 'theme_options_panel');

function manus_user_func(){
    $myurl = site_url().'/wp-admin/users.php';
    ?>
    <script type="text/javascript">window.location.replace("<?php echo $myurl; ?>");</script>
    <?php
}
function manus_user_func_editor(){
    $therole = 'editor';
    $myurl = site_url().'/wp-admin/users.php?role='.$therole;
    ?>
    <script type="text/javascript">window.location.replace("<?php echo $myurl; ?>");</script>
    <?php
}
function manus_user_func_revisor(){
    $therole = 'revisor';
    $myurl = site_url().'/wp-admin/users.php?role='.$therole;
    ?>
    <script type="text/javascript">window.location.replace("<?php echo $myurl; ?>");</script>
    <?php
}
function manus_user_func_autor(){
    $therole = 'author';
    $myurl = site_url().'/wp-admin/users.php?role='.$therole;
    ?>
    <script type="text/javascript">window.location.replace("<?php echo $myurl; ?>");</script>
    <?php
}


?>