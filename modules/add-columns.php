<?php

add_filter( 'manage_posts_columns', 'revealid_add_id_column');
add_action( 'manage_posts_custom_column', 'revealid_id_column_content', 10, 2 );

function revealid_id_column_content( $column, $id ) {
  if( 'revealid_id' == $column ) {
    echo $id;
  }
}

function revealid_add_id_column( $columns ) {
  $checkbox = array_slice( $columns , 0, 1 );
  $columns = array_slice( $columns , 1 );

  $id['revealid_id'] = 'ID';
  
  $columns = array_merge( $checkbox, $id, $columns );
  return $columns;
}

?>