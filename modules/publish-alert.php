<?php

// ALERT BEFORE PUBLISH

add_action('admin_head', 'xxx_admin_hook');

function xxx_admin_hook(){
?>
<script language="javascript" type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#post').submit(function() {
        //alert('Handler for .submit() called.');
        //return false;
        var cfm = confirm("Estimado Autor,\n" + 
            "Una vez seleccionado 'finalizar registro' ya no podrá editar su resumen.");
        if(cfm)
        {
            return true;
        }
    jQuery('#ajax-loading').hide();
    jQuery('#publish').removeClass('button-primary-disabled');

        return false;
    });
});
</script>
<?php
}