<?php

/* --------------- */
// PROCESO EDITORIAL
/* --------------- */


// Si cambia un valor de los paneles de Editor, Revisor y Admin se actualiza el Proceso Editorial

// CHECK STATUS UPDATE AND SEND MAIL


// AUTOR trabajo listo
add_filter('acf/update_value/name=trabajo_listo_para_revision', 'my_check_for_change_borrador', 10, 3);
    
function my_check_for_change_borrador($value, $post_id, $field) {

$correotest = array('juan.gonzalez@nuevaweb.com.mx');
$correoAdmin = 'ltorres@gastro.org.mx';
$correoCC = array('ltorres@gastro.org.mx','juan.gonzalez@nuevaweb.com.mx','juan1688@gmail.com');

                $to = 'juan.gonzalez@nuevaweb.com.mx';
                $subject = 'El trabajo $post_id se guardó';
                $body = "El trabajo $post_id se guardó";
                $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
                wp_mail( $to, $subject, $body, $headers );


  $old_value = get_post_meta($post_id,'trabajo_listo_para_revision');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'borrador' && $old_value[0] != $value && $value == 'Sí') {
                $to = $correoAdmin;
                $subject = 'Se ha registrado un Nuevo Envío.';
                $body = 'Se ha registrado un Nuevo Envío.';
                $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
                wp_mail( $to, $subject, $body, $headers );

                update_field('estatus_editorial', 'nuevo envío', $post_id);
  }
  return $value;
}

// ADMIN asigna editor
add_filter('acf/update_value/name=asignar_editor', 'my_check_for_change_asignar_editor', 10, 3);
    
function my_check_for_change_asignar_editor($value, $post_id, $field) {
  $old_value = get_post_meta($post_id,'asignar_editor');
  $edo_editorial = get_field('estatus_editorial', $post_id);
    if ( $old_value[0] != $value ) {
      if ( $edo_editorial == 'revisión técnica' || $edo_editorial == 'nuevo envío' ) {
              $to = $correoAdmin;
              $subject = 'Se le ha asignado un Trabajo a Editor.';
              $body = 'Se le ha asignado un Trabajo a Editor.';
              $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
              wp_mail( $to, $subject, $body, $headers );

              update_field('estatus_editorial', 'pendiente de editor', $post_id);
      }
    }
  return $value;
}

// EDITOR asigna revisor
add_filter('acf/update_value/name=asignar_revisor', 'my_check_for_change_asignar_revisor', 10, 3);
    
function my_check_for_change_asignar_revisor($value, $post_id, $field) {
  $old_value = get_post_meta($post_id,'asignar_revisor');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'pendiente de editor' && $old_value[0] != $value) {
                $to = $correoAdmin;
                $subject = 'Se le ha asignado un Trabajo a Revisor.';
                $body = 'Se le ha asignado un Trabajo a Revisor.';
                $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
                wp_mail( $to, $subject, $body, $headers );

                update_field('estatus_editorial', 'pendiente de revisor', $post_id);
  }
  return $value;
}
 
// REVISOR califica sugerencia
add_filter('acf/update_value/name=sugerencia', 'my_check_for_change_sugerencia', 10, 3);
    
function my_check_for_change_sugerencia($value, $post_id, $field) {
  $old_value = get_post_meta($post_id,'sugerencia');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'pendiente de revisor' && $old_value[0] != $value) {
        if ( $value != 'Pendiente' ){

                $to = $correoAdmin;
                $subject = 'Revisor ha entregado evaluaciones con sugerencia.';
                $body = 'Revisor ha entregado evaluaciones con sugerencia.';
                $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
                wp_mail( $to, $subject, $body, $headers );

                update_field('estatus_editorial', 'evaluaciones entregadas', $post_id);
        }
  }
  return $value;
}

// EDITOR toma decisión
add_filter('acf/update_value/name=decision_final', 'my_check_for_change_decision_final', 10, 3);
    
function my_check_for_change_decision_final($value, $post_id, $field) {
  $old_value = get_post_meta($post_id,'decision_final');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'evaluaciones entregadas' && $old_value[0] != $value) {
        if ( $value != 'Pendiente' ){

                $to = $correoAdmin;
                $subject = 'Editor ha tomado una decisión en el trabajo.';
                $body = 'Editor ha tomado una decisión en el trabajo.';
                $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
                wp_mail( $to, $subject, $body, $headers );

                update_field('estatus_editorial', 'decisión en curso', $post_id);
        }
  }
  return $value;
}

// ADMINISTRADOR ratifica
add_filter('acf/update_value/name=ratificar_trabajo', 'my_check_for_change_ratificar_trabajo', 10, 3);
    
function my_check_for_change_ratificar_trabajo($value, $post_id, $field) {
  $old_value = get_post_meta($post_id,'ratificar_trabajo');
  $edo_editorial = get_field('estatus_editorial', $post_id);
      if ( $edo_editorial == 'decisión en curso' && $old_value[0] != $value) {
        if ( $value == 'Sí' ){

                $to = $correoAdmin;
                $subject = 'El trabajo ha sido ratificado y liberado al autor.';
                $body = 'El trabajo ha sido ratificado y liberado al autor.';
                $headers = array('Content-Type: text/html; charset=UTF-8','From: Manuscritos AMG <info@amg-envio-manuscritos-cursos-resumenescongreso.mx');
                wp_mail( $to, $subject, $body, $headers );

                update_field('estatus_editorial', 'aceptado', $post_id);
        }
  }
  return $value;
}

?>