<?php

$requireWordpress = require_once("../../../wp-load.php");
if ($requireWordpress) {
  require_once("../../../wp-load.php");
  $whereami = "Local";
} else {
  require_once __DIR__ . '/../../../../wp-load.php';
  $whereami = "Manuscritos";
}

?>

<html>
<head>
  <title>Acceso Directo: <?php echo $whereami; ?></title>
  <style type="text/css">
    .acf-field{
      margin: 10px;
    }
  </style>
  <link rel="stylesheet" href="http://localhost:8888/manuscritos/wp-admin/load-styles.php?c=0&amp;dir=ltr&amp;load%5B%5D=dashicons,admin-bar,common,forms,admin-menu,dashboard,list-tables,edit,revisions,media,themes,about,nav-menus,widgets,site-icon,&amp;load%5B%5D=l10n,buttons,wp-auth-check,media-views&amp;ver=4.8" type="text/css" media="all">
</head>
<body>

<?php acf_form_head(); ?>
<body>
<?php

// $to = 'juan.gonzalez@nuevaweb.com.mx';
// $subject = 'The subject';
// $body = 'The email body content';
// $headers = array('Content-Type: text/html; charset=UTF-8');
 
// wp_mail( $to, $subject, $body, $headers );

    $actual_link = 'http:/'.$_SERVER['REQUEST_URI'];
    $origin = $actual_link;
    // $origin = $_SERVER['HTTP_REFERER'];
    $remove_http = str_replace('http://', '', $origin);
    $split_url = explode('?', $remove_http);
    $get_page_name = explode('/', $split_url[0]);
    $page_name = $get_page_name[1];

    $split_parameters = explode('&', $split_url[1]);
    $split_post_id[0] = explode('=', $split_parameters[0]);
    $origin_post_id = $split_post_id[0][1];

    echo $origin.'<br>';

    if ( $origin_post_id == true){
      echo 'editando el trabajo: '.$origin_post_id.'<br><br>';
    } else {
      echo 'No se ha seleccionado un trabajo, la captura del formulario no surtirá efecto.'.'<br><br><br>';
    }

    $panelArray_manuscritos = array(
      'group_594a980900d2a' => 'asignar editor', 
      'group_594979cc86cad' => 'asignar revisor', 
      'group_5949625e9af0b' => 'panel editor', 
      'group_5942f6edc01ac' => 'panel revisor', 
      'group_594d57992a75e' => 'ratificar trabajo'
    );

    $panelArray_local = array(
      'group_594a980900d2a' => 'asignar editor', 
      'group_594979cc86cad' => 'asignar revisor', 
      'group_5949625e9af0b' => 'panel editor', 
      'group_5942f6edc01ac' => 'panel revisor', 
      'group_594d546905ac5' => 'ratificar trabajo',
      'group_5962f40d26071' => 'popup modal test'
    );

  $arreglo = array(
    'id' => 'acf-form',
    'post_id' => $origin_post_id,
    'field_groups' => array('group_5942f6edc01ac'),
    'submit_value'    => 'Actualizar'
  );

  acf_form($arreglo);

?>


</body>
</html>