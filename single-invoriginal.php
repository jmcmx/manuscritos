<?php include("customwork-template.php"); ?>

<?php

// INDICE

// $categoria_i =      get_post_custom_values( $key = "categoria_i_rio_acf" );
// $categoria_ii =     get_post_custom_values( $key = "categoria_ii_rio_acf" );
// $preferencia =      get_post_custom_values( $key = "preferencia_del_autor_rio_acf" );
// $socio_amg =        get_post_custom_values( $key = "el_autor_principal_es_socio_de_la_amg_rio_acf" );
$nombre =           get_post_custom_values( $key = "nombre_completo_del_autor_principal_io_acf" );
$correo =           get_post_custom_values( $key = "correo_io_acf" );
$institucion =      get_post_custom_values( $key = "institucion_del_autor_principal_io_acf" );
$tiene_coautores =  get_post_custom_values( $key = "el_trabajo_tiene_coautores_io_acf" );
$coautores =        get_post_custom_values( $key = "coautores_io_acf" );

$resumen =          get_post_custom_values( $key = "resumen_en_espanol_io_acf" );
$introduccion =     get_post_custom_values( $key = "introduccion_io_acf" );
$objetivo =         get_post_custom_values( $key = "objetivo_io_acf" );
$material =         get_post_custom_values( $key = "material_y_metodos_io_acf" );
$resultados =       get_post_custom_values( $key = "resultados_io_acf" );
$discusión =        get_post_custom_values( $key = "discusion_io_acf" );
$conclusiones =     get_post_custom_values( $key = "conclusiones_io_acf" );
$conflicto =        get_post_custom_values( $key = "conflicto_de_intereses_io_acf" );
$financiamiento =   get_post_custom_values( $key = "financiamiento_io_acf" );
$referencias =      get_post_custom_values( $key = "referencias_io_acf" );

$a_figura =         get_post_custom_values( $key = "adjuntar_figura_io_acf" );
$a_tabla =          get_post_custom_values( $key = "adjuntar_tabla_io_acf" );

?>

<body>

    <div class="worksheet-heading noprint">
        <h1><?php the_title(); ?></h1>
        <button onclick="myFunction()">Ver en PDF</button>
        <!-- <div class="worksheet-panel" style="padding: 20px; background-color: white; text-align: center;">
        </div> -->
    </div>

    <div class="worksheet animated fadeIn">
        <p>
            <span>Título del trabajo:</span><?php the_title_attribute(); ?><br><span>Autor:</span> <?php echo $nombre[0];?> <span>Coautores:</span> <?php if( have_rows('coautores_rio_acf')){ while ( have_rows('coautores_rio_acf') ) : $row = the_row(); the_sub_field('coautores'); echo ", "; endwhile; } else { echo "Sin Coautores"; } ?> <span>Institución:</span> <?php echo $institucion[0];?> <span>Correo:</span> <?php echo $correo[0];?><br><span>Resumen:</span> <?php echo $resumen[0];?><br><span>Introducción:</span> <?php echo $introduccion[0];?><br><span>Objetivo:</span> <?php echo $objetivo[0];?><br><span>Material y Métodos:</span> <?php echo $material[0];?><br><span>Resultados:</span> <?php echo $resultados[0];?><br><span>Discusión:</span> <?php echo $discusión[0];?><br><span>Conclusiones:</span> <?php echo $conclusiones[0];?>
        </p>   
    </div>
    <div class="worksheet animated fadeIn">
        <p>
            <span>Imagen:<br></span>
            <?php
                if ($a_figura[0]){
                    $workImage = $a_figura[0];
                    ?>
                    <a href="<?php echo wp_get_attachment_url($a_figura[0]); ?>"><?php echo wp_get_attachment_image($a_figura[0]); ?></a>
                    <?php
                } else {
                    echo "Este trabajo no contiene imagen.";
                }
            ?>
            <br>
            <span>Tabla:<br></span>
            <?php
                if ($a_tabla[0]) {
                $workTable = $a_tabla[0];
                ?>
                    <a href="<?php echo wp_get_attachment_url($a_tabla[0]); ?>">Ver Tabla</a>
                <?php
                } else {
                    echo "Este trabajo no contiene tabla.";
                }
            ?>
        </p>  

    </div>

    <?php wp_footer(); ?>

    <script type="text/javascript">
        function myFunction() {
            window.print();
        }    
    </script>
</body>
</html>